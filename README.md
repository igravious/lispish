
# Lispish READ_ME

## Overview

trying to (1st) build a tiny untyped† lisp-like in ruby language from make-a-lisp

then to (2nd) add simple sorts of type inferencing with μKanren 

then to (3rd) add dependent types

- working reference in repl.rb

- dev branch zeta.rb

† It has been observed that, paradoxically, if there is no type system – that
is to say, if the language is untyped then in fact there *is* one type (in that
everything has the same non-type) :/

`ruby _test_parslet.rb` shows sample parslet parse trees

## Running Various Components

- testing the language
  `./runtest.py ./tests/lispish.mal ./xrun`
  `./runtest.py ./tests/fispish.mal ./xrun`

  There's an script `badger` for running them.

- testing the codebase (and coverage)
  not working yet

  Using a script called `stoat`

- warnings and debuggings
  -w turns on warnings
  -d turns on debugging
  we use shush to fix noisiness, see shush

- command line repl
  `ruby ish.rb`

## Description

Parslet-powered parser `dynamic.rb` parses the input string (I think it's line by line at the moment)
Then it's evaluated using the algo in `untyped_object.rb` (in the `proving` folder)
These are both handled by `qepl.rb`

The parser was way easier than the evaluator; initially I had a bonkers regex 
driven recursive descent, it was awful. The parser took a bit of time to massage into doing 
the righ thing. The evaluator, I couldn't reason it into correct behaviour, I had to create 
a lambda expression test suite and tweak for weeks.

Describe everything I looked at in LINKS.md

.ish file extension

## Proximate

Put on the web. Loads of comments. Loads of docs. Test suite. Code coverage.
Different evaluation strategies. Different flavours of basic language?

## Ultimate Vision

link with Peircean triads and Semantic Web RDF

## 

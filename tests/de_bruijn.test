;;;
;;; Have relied on the wonderful tool:
;;; http://www.itu.dk/people/sestoft/lamreduce/lamframes.html
;;; http://raspi.itu.dk/cgi-bin/lamreduce?action=print+abbreviations
;;;
;;; A suite of test cases is needed, contrary to what Andrej Bauer says:
;;; https://cs.stackexchange.com/questions/9001/test-cases-for-λ-calculus
;;; https://stackoverflow.com/questions/15690904/lambda-calculus-expression-test-bed
;;;
;;; Any bugs to @igravious
;;;
;;; Working translations for Haskell would be appreciated.
;;; http://dev.stephendiehl.com/fun/003_lambda_calculus.html
;;;

;;; Used as superscripts in some test-cases
;;; 0 in Ref    º   Alt-Gr 0
;;; 1 in Ref    ª   Alt-Gr 9
;;; 2 in Ref    ˇ   Alt-Gr v

;;; Used as superscripts in other test-cases
;;; https://en.wikipedia.org/wiki/Unicode_subscripts_and_superscripts
;;; :UnicodeSearch superscript
;;; 0 in Ref    ⁰
;;; 1 in Ref    ¹
;;; 2 in Ref    ²
;;; 3 in Ref    ³
;;; 4 in Ref    ⁴
;;; 5 in Ref    ⁵

;;; symbol used for α conversion: •

;;; Some examples
;;; https://codegolf.stackexchange.com/questions/284/write-an-interpreter-for-the-untyped-lambda-calculus

;;; this file uses standard lambda notation syntax
;;; combinators are defined elsewhere (in a so-called Prelude file)
;;; the standard lambda notation syntax does not support definitions/aliases

;;; this file checks are De Bruijn code

;; =====
;; Ref a
;;
;;     a
;;
;; depends on whether a bare term is viewed as a variable or literal
;;
a
;=>-1 # -1

;; =======
;; Ref a b
;;
;;     a b
;;
;; ? [a b]
;;
(a b)
;=>-1 -2 # -1 -2

;; =============
;; Ref (a (b c))
;;
;;     a (b c)
;;
;; ? [a b c]
;;
(a (b c))
;=>-1 (-2 -3) # -1 (-2 -3)

;; =============
;; Ref ((a b) c)
;;
;;     a b c
;;
;; ? [a b c]
;;
((a b) c)
;=>-1 -2 -3 # -1 -2 -3

;; =========
;; Ref a b c
;;
;;     a b c
;;
;; ? [a b c]
;;
(a b c)
;=>-1 -2 -3 # -1 -2 -3

;; ===============
;; Ref \ y . x y
;;
;;     \y.x y
;;
\ y . x y
;=>λ.-1 0 # λ.-1 0

;; ===============
;; Ref (\ y . x y)
;;
;;     \y.x y
;;
(\ y . x y)
;=>λ.-1 0 # λ.-1 0

;; =================
;; Ref (\x.x) (\y.y)
;;
;;     [(\x.x) (\y.y)]º
;; ==> [\y.y]º
;;
(\ x . x) (\ y . y)
;=>λ.0 # λ.0

;; ===================
;; Ref ((\x.x) (\y.y))
;;
;;     [(\x.x) (\y.y)]º
;; ==> [\y.y]º
;;
((\ x . x) (\ y . y))
;=>λ.0 # λ.0

;; =====================
;; Ref (\x. (\y. (x y)))
;;
;;     \x.\y.x y
;;
;; Not sure about this one.
;;
(\ x . \ y . x y)
;=>λ.λ.1 0 # λ.λ.1 0

;; =====================
;; Ref (\x. (\y. (x y)))
;;
;;     \x.\y.x y
;;
;; Nor this.
;;
(\ x . (\ y . x y))
;=>λ.λ.1 0 # λ.λ.1 0

;; =================
;; Ref (\x.(\y.z)) y
;;
;;     [(\x.\y.z) (y)]º
;; ==> [\y.z]º
;;
((\ x . (\ y . z)) y)
;=>λ.-1 # λ.-1

;; =================
;; Ref (\x.(\y.x)) y
;;
;;     [(\x.\y.x) (y)]º
;; ==> [\y1.y]º
;;
((\ x . (\ y . x)) y)
;=>λ.-1 # λ.-1

;; ======================
;; Ref (\x.(\y.(\y.y))) y
;;
;;     [(\x.\y.\y.y) (y)]º
;; ==> [\y.\y.y]º
;;
((\ x . (\ y . (\ y . y))) y)
;=>λ.λ.0 # λ.λ.0

;; ==================
;; Ref (\x.x(\z.z)) y
;;
;;     [(\x.x (\z.z)) (y)]º
;; ==> [y (\z.z)]º
;;
;; ? [y (λ [z] z)]
;;
((\ x . x (\ z . z)) y)
;=>-1 λ.0 # -1 λ.0

;; ==================
;; Ref (\x.x(\x.x)) y
;;
;;     [(\x.x (\x.x)) (y)]º
;; ==> [y (\x.x)]º
;;
;; ? [y (λ [x] x)]
;;
((\ x . x (\ x . x)) y)
;=>-1 λ.0 # -1 λ.0

;; =========================
;; Ref ((\x. (\y. y x)) y) z
;;
;;     [(\x.\y.y x) (y)]º z
;; ==> [(\y1.y1 y)º (z)]ª
;; ==> [z y]ª
;;
;; ? [z y]
;;
(((\ x . (\ y . (y x))) y) z)
;=>-1 -2 # -2 -1

;; =====================
;; Ref (\x. (\y. y x)) y
;;
;;     [(\x.\y.y x) (y)]º
;; ==> [\y1.y1 y]º
;;
;; alpha-rename   (\x. (\y1. y1 x)) y   beta-reduce   (\y1. y1 y)
;;
((\ x . (\ y . (y x))) y)
;=>λ.0 -1 # λ.0 -1

;; ======
;; Ref $K
;;
;; K Combinator
;; defined in ~/.lispishrc
;;
(\ x . \ y . x)
;=>λ.λ.1 # λ.λ.1

;; ======
;; Ref $S
;;
;; S Comnbinator
;; defined in ~/.lispishrc
;;
(\ f . \ g . \ x . f x (g x))
;=>λ.λ.λ.2 0 (1 0) # λ.λ.λ.2 0 (1 0)

;; ==================
;; Ref (($S $K $K) x)
;;
;; [(\f.\g.\x.f x (g x)) ($K)]0 $K x
;; ==> [(\g.\x.$K x (g x))0 ($K)]1 x
;; ==> [(\x.$K x ($K x))1 (x)]2
;; ==> [[(\x.\y.x) (x)]3 ($K x)]2
;; ==> [[(\y.x)3 ($K x)]4]2
;; ==> [x4]2
;;
;; build $I out of $S's and $K's
;; that's not a possessive '
;; I'm trying to say Essǝz and Kayz
;;
((S K K) x)
;=>-1 # -1

;; ======
;; Ref I
;;
;; Identity Combinator
;; defined in ~/.lispishrc
;;
(\ x . x)
;=>λ.0 # λ.0

;; =========================
;; Ref $id ($id (\z. $id z))
;;
;; Page 56 of TaPL
;;
;;     [(\x.x) ($id (\z.$id z))]º
;; ==> [[(\x.x) (\z.$id z)]ª]º
;; ==> [[\z.[(\x.x) (z)]ˇ]ª]º
;; ==> [[\z.zˇ]ª]º
;;
(I (I (\ z . I z)))
;=>λ.0 # λ.0

;; ====
;; Ref (\x.x)((\x.x)(\z.(\x.x)z))
;;
;; same thing, no spaces – lexer test
;;
(\x.x)((\x.x)(\z.(\x.x)z))
;=>λ.0 # λ.0

;; ======
;; Ref $D
;;
;; defined in ~/.lispishrc
;;
(\ x . (x x))
;=>λ.0 0 # λ.0 0

;; ======
;; Ref $F
;;
;; “Error: alias F is not a closed term and won't be registered” (cannot define in ~/.lispishrc)
;;
;;
(\ f . (f (f y)))
;=>λ.0 (0 -1) # λ.0 (0 -1)

;; ================
;; Ref ($D ($F $I))
;;
;; https://stackoverflow.com/a/16332815
;;
(D ((\ f . (f (f y))) I))
;=>-1 -1 # -1 -1

;; =====================
;; Ref ((\x.\y.(y x)) (y w))
;;
;; [(\x.\y.y x) (y w)]º
;; ==> [\y1.y1 (y w)]º
((\ x . \ y . (y x)) (y w))
;=>λ.0 (-1 -2) # λ.0 (-1 -2)

;; =====================
;; Ref ((\x. \y. x y) a)
;;
;;     [(\x.\y.x y) (a)]º
;; ==> [\y.a y]º
;;
;; depends on binding strategy
;; (normal order)
;;
((\ x . \ y . (x y)) eight)
;=>λ.-1 0 # λ.-1 0

;; =====================
;; Ref ((\x. \y. x y) a)
;;
;;     [(\x.\y.x y) (a)]º
;; ==> [\y.a y]º
;;
;; depends on binding strategy
;; (normal order)
;;
((\ x . (\ y . (x y))) eight)
;=>λ.-1 0 # λ.-1 0

;; ==============================
;; Ref ((\x.(\y.(\z.(x y z)))) b)
;;
;;     [(\x.\y.\z.x y z) (b)]º
;; ==> [\y.\z.b y z]º
;;
;; presumably ditto
;;
((\ x . \ y . \ z . (x y z)) seven)
;=>λ.λ.-1 1 0 # λ.λ.-1 1 0

;; =======================
;; Ref ((\x. \y. x y) z w)
;;
;;     [(\x.\y.x y) (z)]º w
;; ==> [(\y.z y)º (w)]ª
;; ==> [z w]ª
;;
;; um? (does this need to be a church numeral)
;;
((\ x . \ y . (x y)) seven nine)
;=>-1 -2 # -1 -2

;; =====
;; Ref -
;;
((λ x . x) (λ y . (λ z . z)))
;=>λ.λ.0 # λ.λ.0

;; =====
;; Ref -
;;
(λ x . ((λ y . y) x))
;=>λ.0 # λ.0

;; =====
;; Ref -
;;
((λ x . (λ y . x)) (λ a . a))
;=>λ.λ.0 # λ.λ.0

;; =====
;; Ref -
;;
(((λ x . (λ y . x)) (λ a . a)) (λ b . b))
;=>λ.0 # λ.0

;; =====
;; Ref -
;;
((λ x . (λ y . y)) (λ a . a))
;=>λ.0 # λ.0

;; =====
;; Ref -
;;
(((λ x . (λ y . y)) (λ a . a)) (λ b . b))
;=>λ.0 # λ.0

;; need more λ […] … examples

;; =========================
;; Ref ((\x. \y. x y) z w v)
;;
;;     [(\x.\y.x y) (z)]º w v
;; ==> [(\y.z y)º (w)]ª v
;; ==> [z w]ª v
;;
;; depends on binding strategy
;; (which in lispish is what by default exactly?)
;;
((λ [x y] (x y)) seven eight nine)
;=>-1 -2 -3 # -1 -2 -3

;; =========================
;; Ref ((\t . (\f . f)) a b)
;;
;;     [(\t.\f.f) (a)]º b
;;     ==> [(\f.f)º (b)]ª
;;     ==> bª
;;
;; -1 cuz named -> nameless of one var
;; -1 # -2 is correct
;;
(((\ t . (\ f . f)) one) two)
;=>-1 # -2

;; =========================
;; Ref ((\t . (\f . t)) a b)
;;
;;     [(\t.\f.t) (a)]º b
;;     ==> [(\f.a)º (b)]ª
;;     ==> aª
;;
(((\ t . (\ f . t)) one) two)
;=>-1 # -1

;; ========
;; Ref $tru
;;
(\ x . \ y . x)
;=>λ.λ.1 # λ.λ.1

;; ========
;; Ref $fls
;;
(\ x . \ y . y)
;=>λ.λ.0 # λ.λ.0

;; =====
;; Ref -
;;
(True v w)
;=>-1 # -1

;; =====
;; Ref -
;;
((True v) w)
;=>-1 # -1

;; =====
;; Ref -
;;
(True (v w))
;=>λ.-1 -2 # λ.-1 -2

;; =====
;; Ref -
;;
;; -1 cuz named -> nameless of one var
;; -1 # -2 is correct
;;
(False v w)
;=>-1 # -2

;; =========
;; Ref $test
;;
(\ l . \ m . \ n . l m n)
;=>λ.λ.λ.2 1 0 # λ.λ.λ.2 1 0

;; =====
;; Ref -
;;
((((λ l . (λ m . (λ n . l m n))) True) v) w)
;=>-1 # -1

;; ======================================
;; Ref (\ l . \ m . \ n . l m n) $tru v w
;;
;;     [(\l.\m.\n.l m n) ($tru)]⁰ v w
;; ==> [(\m.\n.$tru m n)⁰ (v)]¹ w
;; ==> [(\n.$tru v n)¹ (w)]²
;; ==> [[(\x.\y.x) (v)]³ w]²
;; ==> [[(\y.v)³ (w)]⁴]²
;; ==> [v⁴]²
(λ l . (λ m . (λ n . l m n))) True v w
;=>-1 # -1

;; =====
;; Ref -
;;
(((Test True) v) w)
;=>-1 # -1

;; =====
;; Ref -
;;
Test True v w
;=>-1 # -1

;; ============================
;; Ref - possible infinite loop
;;
;; (This is an example of an 
;; expression which does not 
;; normalize if you evaluate 
;; the arguments before the 
;; function call.)
;;
(((λ x . (λ y . x)) (λ a . a)) ((λ x . (x x)) (λ x . (x x))))
;=>λ.0 # λ.0

;; ============================
;; Ref - 2^3 in Church numerals
;;
((λ a . (λ b . (a (a (a b))))) (λ c . (λ d . (c (c d)))))
;=>λ.λ.1 (1 (1 (1 (1 (1 (1 (1 0))))))) # λ.λ.1 (1 (1 (1 (1 (1 (1 (1 0)))))))

;; ====
;; Ref - https://stackoverflow.com/questions/34140819/lambda-calculus-reduction-steps
;; 6 beta reduction steps
;; 
;;     [(\x.\y.\z.x y z) (\x.x x)]⁰ (\x.x) x
;; ==> [(\y.\z.(\x.x x) y z)⁰ (\x.x)]¹ x
;; ==> [(\z.(\x.x x) (\x.x) z)¹ (x)]²
;; ==> [[(\x.x x) (\x.x)]³ x]²
;; ==> [[[(\x.x) (\x.x)]⁴]³ x]²
;; ==> [[[(\x.x)⁴]³ (x)]⁵]²
;; ==> [x⁵]²
;;
(λx y z.x y z) (λx.x x) (λx.x) (x)
;=>-1 # -1



# run me with bin/rspec

SOURCE = File.join('..', 'src')

require_relative File.join(SOURCE, 'include')
require_relative File.join(SOURCE, 'muh')

# https://relishapp.com/rspec/rspec-expectations/docs/built-in-matchers

describe SurrealNumber do
	subject { 0.to_surreal }

	it 'has correct Conway representation' do
		# https://en.wikipedia.org/wiki/Surreal_number
		puts 'zero!'
		p subject
		expect(subject.conway).to eq(CONWAY_)
	end

	it 'creates a surreal number from 0' do
		expect(subject.class).to eq(SurrealNumber)
	end

	it 'double checks that 0.to_surreal == NAUGHT' do
		# puts "NAUGHT = #{subject.quote}"
		expect(subject).to eq(NAUGHT_)
	end

	it 'verifies addition' do
		poz_one = subject + 1
		poz_two = poz_one + 1
		poz_tri = poz_two + 1
		expect(poz_one).to eq(1.to_surreal)
		expect(poz_two).to eq(2.to_surreal)
		expect(poz_tri).to eq(3.to_surreal)
		expect(NAUGHT_<=poz_one).to be true
		expect(NAUGHT_<=poz_two).to be true
		expect(poz_one<=poz_two).to be true
		expect(NAUGHT_<=poz_tri).to be true
		expect(poz_one<=poz_tri).to be true
		expect(poz_two<=poz_tri).to be true
		expect(NAUGHT_>=poz_one).to be false
		expect(NAUGHT_>=poz_two).to be false
		expect(poz_one>=poz_two).to be false
		expect(NAUGHT_>=poz_tri).to be false
		expect(poz_one>=poz_tri).to be false
		expect(poz_two>=poz_tri).to be false
	end

	it 'verifies subtraction' do
		neg_one = subject - 1
		neg_two = neg_one - 1
		neg_tri = neg_two - 1
		expect(neg_one).to eq(-1.to_surreal)
		expect(neg_two).to eq(-2.to_surreal)
		expect(neg_tri).to eq(-3.to_surreal)
		expect(NAUGHT_>=neg_one).to be true
		expect(NAUGHT_>=neg_two).to be true
		expect(neg_one>=neg_two).to be true
		expect(NAUGHT_>=neg_tri).to be true
		expect(neg_one>=neg_tri).to be true
		expect(neg_two>=neg_tri).to be true
		expect(NAUGHT_<=neg_one).to be false
		expect(NAUGHT_<=neg_two).to be false
		expect(neg_one<=neg_two).to be false
		expect(NAUGHT_<=neg_tri).to be false
		expect(neg_one<=neg_tri).to be false
		expect(neg_two<=neg_tri).to be false
	end

	it 'goes over boundary' do
		poz_one = subject.inc
		poz_two = poz_one.inc
		neg_one = subject.dec
		neg_two = neg_one.dec

		# puts('=====')
		expect(poz_one).to eq(poz_two-1)
		# puts('=====')
		expect(NAUGHT_).to eq(poz_two-2)
		# puts('=====')
		expect(neg_one).to eq(poz_two-3)
		# puts('=====')
		expect(poz_one).to eq(neg_two+3)
		# puts('=====')
		expect(NAUGHT_).to eq(neg_two+2)
		# puts('=====')
		expect(neg_one).to eq(neg_two+1)
		# puts('=====')
	end

	it 'verifies addition using inc()' do
		poz_one = subject.inc
		# puts "::: #{poz_one.conway}"
		poz_two = poz_one.inc
		poz_tri = poz_two.inc
		# puts "ONE = #{one.quote}"
		# puts "1.to_surreal = #{1.to_surreal.quote}"
		expect(poz_one).to eq(1.to_surreal)
		expect(poz_two).to eq(2.to_surreal)
		expect(poz_tri).to eq(3.to_surreal)
		expect(NAUGHT_<=poz_one).to be true
		expect(NAUGHT_<=poz_two).to be true
		expect(poz_one<=poz_two).to be true
		expect(NAUGHT_<=poz_tri).to be true
		expect(poz_one<=poz_tri).to be true
		expect(poz_two<=poz_tri).to be true
		expect(NAUGHT_>=poz_one).to be false
		expect(NAUGHT_>=poz_two).to be false
		expect(poz_one>=poz_two).to be false
		expect(NAUGHT_>=poz_tri).to be false
		expect(poz_one>=poz_tri).to be false
		expect(poz_two>=poz_tri).to be false
	end

	it 'verifies subtraction using dec()' do
		neg_one = subject.dec
		neg_two = neg_one.dec
		neg_tri = neg_two.dec
		# puts "ONE = #{one.quote}"
		# puts "1.to_surreal = #{1.to_surreal.quote}"
		expect(neg_one).to eq(-1.to_surreal)
		expect(neg_two).to eq(-2.to_surreal)
		expect(neg_tri).to eq(-3.to_surreal)
		expect(NAUGHT_>=neg_one).to be true
		expect(NAUGHT_>=neg_two).to be true
		expect(neg_one>=neg_two).to be true
		expect(NAUGHT_>=neg_tri).to be true
		expect(neg_one>=neg_tri).to be true
		expect(neg_two>=neg_tri).to be true
		expect(NAUGHT_<=neg_one).to be false
		expect(NAUGHT_<=neg_two).to be false
		expect(neg_one<=neg_two).to be false
		expect(NAUGHT_<=neg_tri).to be false
		expect(neg_one<=neg_tri).to be false
		expect(neg_two<=neg_tri).to be false
	end

	it 'converts back to integer properly' do
		neg_one = subject.dec
		neg_two = neg_one.dec
		neg_tri = neg_two.dec
		poz_one = subject.inc
		poz_two = poz_one.inc
		poz_tri = poz_two.inc

		expect(NAUGHT_.to_i).to eq(0)
		expect(poz_one.to_i).to eq(1)
		expect(poz_two.to_i).to eq(2)
		expect(poz_tri.to_i).to eq(3)
		expect(neg_one.to_i).to eq(-1)
		expect(neg_two.to_i).to eq(-2)
		expect(neg_tri.to_i).to eq(-3)
	end

=begin
	it 'only handles integers for the moment' do
		# { 0 | 1 } = 1/2
		# https://en.wikipedia.org/wiki/Surreal_number
		one_half = NAUGHT_ ^ (NAUGHT_ ^ [])
		# curly for `raise_error'
		expect { one_half.to_i }.to raise_error(Math::DomainError) # 'Surreal number is not an integer'
	end
=end
end

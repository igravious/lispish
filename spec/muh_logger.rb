
# run me with bin/rspec

SOURCE = File.join('..', 'src')

require_relative File.join(SOURCE, 'muh')

# $: << '../src'
# require 'muh_logger'
# require 'logger'

describe MuhLogger do
	let(:logger1) { MuhLogger.instance }
	let(:logger2) { MuhLogger.instance }

	it 'only creates one logger' do
		expect(logger1.object_id).to eq(logger2.object_id)
	end

	subject { MuhLogger.instance }

	it 'yoes' do
		subject.info('yo!')
	end

	it 'levels' do
		subject.monochrome
		expect(subject.level).to eq(Logger::INFO)
	end
end

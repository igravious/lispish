# frozen_string_literal: true

# glob all of reader
# top level module
module Lispish
  # this namespace
  module Reader
    Lispish::Include.globular
  end
end

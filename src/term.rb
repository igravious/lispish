# frozen_string_literal: true

# glob all of term
# top level module
module Lispish
  # this namespace
  module Term
    Lispish::Include.globular
  end
end

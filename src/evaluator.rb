# frozen_string_literal: true

# glob all of eval
# top level module
module Lispish
  # this namespace
  module AST
    # private_class_method

    # str  is the message
    #
    # tree is the parse tree
    #
    def self.malformed(str, tree)
      puts 'mal'
      raise "malformed – #{str}: #{tree.inspect}"
    end

    Lispish::Include.globular
  end
end

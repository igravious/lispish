# frozen_string_literal: true

# require 'awesome_print'
# ap ENV['INPUTRC']
# exit

# https://xn--wxa.sahos.org/pages/info/main

module Lispish
  VERSION = 'Lispish 0.6 (mumbling mink)'
end

# foofoo
require_relative 'include'

# hold hands with pry – config
require_relative 'my_pry'

# http://ruby-doc.org/stdlib-1.9.3/libdoc/readline/rdoc/Readline.html
require_relative 'muh'

# read-eval-print-(loop/once)
require_relative 'repl'

IGOR = ARGV.dup

# parse options
# https://docs.ruby-lang.org/en/2.5.0/OptionParser.html
require 'optparse'

# TODO: put into config.rb
options = {}
options[:execute] = []
options[:tree] = false
options[:prelude] = true
features = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: example.rb [options]'

  opts.on('-v', '--[no-]verbose', TrueClass, 'Run verbosely') do |v|
    options[:verbose] = v
    $VERBOSE = v
  end
  opts.on('-d', '--[no-]debug', TrueClass, 'Run in debug mode, also sets verbose') do |d|
    options[:debug] = d
    options[:verbose] = d
    $DEBUG = d
    $VERBOSE = d
  end
  opts.on('-t') do
    options[:tree] = true
  end
  opts.on('-p') do
    options[:prelude] = false
  end
  opts.on('-s STRATEGY', '--strategy STRATEGY', Integer, 'Pick a strategy') do |s|
    features[:strategy] = s - 1
  end
  opts.on('--version') do
    puts VERSION
    exit 0
  end
  opts.on('-e CODE', '--execute CODE', "Execute a chunk of code. Several -e's allowed. Omit [programfile]") do |execute|
    options[:execute].push(execute)
  end
  opts.on('--[no-]feature FEATURE', 'Switch on (or off) a language feature.') do |feature|
    case feature
    when 'with-assignment'
      options[:calculus] = CalculusWithAssignment # named or nameless ?
      features[:named] = true
    when 'standard-named'
      features[:named] = true
    when 'standard-nameless'
      features[:nameless] = true
    when 'standard-surreal'
      features[:surreal] = true
    when 'double-check-nameless'
      features[:nameless] = true
      features[:double_check] = true
    when 'double-check-surreal'
      features[:surreal] = true
      features[:double_check] = true
    end
  end
  opts.on('--show-features', 'List language features.') do |_|
    puts "\tfeature            description"
    puts "\t-------            -----------"
    puts "\twith-assignment    Accept terms with the form `Y ← λf.((λx.f (x x)) (λx.f (x x)))'"
    exit 0
  end
end.parse!

# defaults to nameless at the moment
features[:strategy] = 0 unless features.key?(:strategy) # CONST
options[:calculus] =
  if options.key?(:calculus)
    options[:calculus].new(features)
  else
    StandardLambdaCalculus.new(features)
  end

# what's the difference between verbosity and debug statements ?
# MuhLogger.instance.level_error unless ARGV[0] == '-v' or ARGV[0] == '-d'
MuhLogger.instance.level_error unless $VERBOSE or $DEBUG

# newish -> syntax (wish there was a unicode of this)
# promote loggind methods
module Lispish
  INF = ->(msg) { MuhLogger.instance.info(msg) }
  WRN = ->(msg) { MuhLogger.instance.warn(msg) }
  DBG = ->(msg) { MuhLogger.instance.debug(msg) }
  ERR = ->(msg) { MuhLogger.instance.error(msg) }
end

L = Lispish

L::INF["ARGV = #{IGOR.inspect}"]
L::INF["OPTS = #{options.inspect}"]

# target
#
# https://github.com/MaiaVictor/caramel

# specify a language variant

# https://wiki.archlinux.org/index.php/XDG_Base_Directory
# use a config file: $XDG_CONFIG_HOME

# FIXME: There is only one environment :( (Singleton it)

# encapsulate evaluation abortion

def set_abort_eval
  $abort_eval = true
end

def reset_abort_eval
  $abort_eval = false
end

def abort_eval?
  $abort_eval
end

# All this needs testing
trap('INT') do
  if @running
    # prompt('breaking … ^C')
    puts
    # raise Lispish::Abort
    set_abort_eval # global variable works better than instance variable for trapping interrupt
  else
    Lispish::Readline::prompt('^C')
    # do nothing
  end
end

reset_abort_eval # initiialize
lang = options[:calculus]
NEED_NEWLINE = true

require_relative 'command'

def execute_line(lang, line)
  begin
    require 'timeout'
    output = ''
    Binding.interrupted = false
    status = Timeout::timeout(600) {
      @running = true

        beginning_time = Time.now
        output = REP[lang, line]
        end_time = Time.now
        L::INF["Took #{end_time-beginning_time} seconds to complete."]

      @running = false
    }
    L::DBG["Timeout status: #{status}"]
  rescue Timeout::Error
    @running = false
    L::WRN["execution expired: does `#{line}' terminate?"] unless Binding.interrupted
    output = '!'
  rescue Lispish::Abort
    @running = false
    L::WRN['user interrupted']
    reset_abort_eval # reset it ?
    output = '!'
  rescue => e # share rescue handler
    @running = false
    L::ERR[e]
    output = '*ERROR*'
  end
  # puts __FILE__ + ':' + __LINE__.to_s
  # puts output.class
  # puts output.length
  puts output unless output.blank?
end

CLI_REGEX = /^\s*([\.\?!][[:alpha:]]*)\s*(.*)$/

begin
  if options[:execute].length > 0 
    options[:execute].each do |line|
      execute_line(lang, line)
    end
  else
    DEFAULT_FILE = './.prelude'
    prelude = "`#{DEFAULT_FILE}'"
    if options[:prelude]
      if File.exist?(DEFAULT_FILE)
        L::INF["Reading prelude file: #{prelude}"]
        File.open(DEFAULT_FILE) do |f|
          begin
            am_i_debugging = $DEBUG
            $DEBUG = false # switch off debugging while ingesting Prelude
            L::INF['Switching off $DEBUG, you may not want this!']
            REPL[CalculusWithAssignment.new, f]
            $DEBUG = am_i_debugging
          rescue => e # share rescue handler
            p e
            L::ERR[e.message]
            L::ERR["… in file #{prelude}"]
            system(Lispish::Readline::STTY_SAVE); # puts ''; # should be a fn in readline ?
            exit 2 # terminsated with error while processing Prelude
          end
          L::INF['Well, that went well.']
        end
      else
        L::WRN["Could no find prelude file: #{prelude}"]
      end
    else
      L::INF["You directed me to ignore the prelude file: #{prelude}"]
    end

    L::INF[Lispish::VERSION]
    L::INF['^D to quit (for the moment)']

    first_time = true
    @running = false
    # TODO: catch interrupt and enable completion
    # interrupt doesn't stop lambda eval? why?
    while (line = Lispish::Readline::readline)
      if first_time
        L::INF["#<Encoding:#{line.encoding}>"]
        first_time = false
      end
      if (line =~ CLI_REGEX).nil?
        # p $~
        execute_line(lang, line)
      else
        # p $~
        cmd_name = $1
        rest_of_line = $2
        if Lispish::Command::am_i_a_command?(cmd_name)
          Lispish::Command::exec_command(cmd_name, lang, rest_of_line) # each command is responsible for parsing $2 for the moment
        else
          puts '? unknown command my liege …'
        end
      end
    end
    printf '^D' if line.nil?
    puts '' if NEED_NEWLINE
  end
rescue StandardError => e # FIXME: dev mode
  L::INF['Should not get here, should already have been caught and processed !!!']
  L::INF['This is what you have tests/specs for Anto']

  puts " <!-- #{e.class}: #{e} -->"
  puts
  puts "\t#{e.backtrace.join("\n\t")}"
  puts
  puts " <!-- #{e.class}: Lispish go bye-bye after this -->"

  if respond_to? :shush # don't need the rigmarole of binding.ish
    shush { binding.pry }
  else
    binding.pry
  end

  exit 1 # Something went terribly wrong - ENUM these :)
end
system(Lispish::Readline::STTY_SAVE); # puts ''; # should be a fn in readline ?
exit 0 # Normal exit

# frozen_string_literal: true

# project outer namespace
module Lispish

  # local namespace
  module Command

    # map command names to the description and code
    @cmd_dict = {}

    # add a command !
    def self.add_command(cmd_name, cmd_desc, &cmd_code)
      @cmd_dict[cmd_name] = {
        description: cmd_desc,
        code: cmd_code
      }
    end

    # and a dot command !
    def self.add_dot_command(cmd_name, cmd_desc, &cmd_code)
      add_command('.' + cmd_name, cmd_desc, &cmd_code)
    end

    # am i a command ?
    def self.am_i_a_command?(cmd_name)
      @cmd_dict.key?(cmd_name)
    end

    # last command typed
    @last_cmd = ''

    # execute a command 
    def self.exec_command(cmd_name, l, rest_of_line)
      # remember last command
      @last_cmd = cmd_name unless cmd_name == '.' # have ignorelist
      # invoke the command
      @cmd_dict[cmd_name][:code].call(l, rest_of_line)
    end

    # repeat last command
    add_command('.', 'Repeat last command') { |l, rest_of_line|
      @cmd_dict[@last_cmd][:code].call(l, rest_of_line) unless @last_cmd.blank?
    }

    # display back to the user the lambda expression they have entered
    add_dot_command('show', 'Show lambda expression') { |l, rest_of_line|
      puts l.READ($repl_env, rest_of_line) unless rest_of_line.blank?
    }

    # display back to the user the lambda expression they have entered
    add_dot_command('type', 'Show lambda expression') { |l, rest_of_line|
      term = l.READ($repl_env, rest_of_line) # unless rest_of_line.blank?
      # term.to_type.to_s – expressions have types, terms don't have types as such
      if term.nil?
        puts '#<Lispish::NilTerm>'
      else
        p term
      end
    }

    # display lambda expression as an ascii tree
    add_dot_command('tree', 'Display lambda expression as an ascii tree') { |l, rest_of_line|
      draw_tree(l.READ($repl_env, rest_of_line)) unless rest_of_line.blank?
    }

    # add_dot_command('tree', Proc.new { |l, rest_of_line|
    #   puts l.READ($repl_env, rest_of_line).to_tree unless rest_of_line.blank?
    # })


    # compact inspection of lambda expression Ruby runtime object
    add_dot_command('inspect', 'Compact inspection of lambda expression Ruby runtime object') { |l, rest_of_line|
      puts(l.READ($repl_env, rest_of_line).inspect {}) # unless rest_of_line.blank?
    }

    # the persistent context within which a lambda expression is executed
    add_dot_command('context', 'The persistent context within which a lambda expression is executed') { |l, rest_of_line|
      if rest_of_line.blank? # show 'em all
        $repl_env.each{ |k, v| puts "#{k} ← #{v}"}
      else
        sym = rest_of_line.to_sym
        if $repl_env.key?(sym) # make this into a class to get rid of global var
          puts "#{rest_of_line} ← #{$repl_env[sym]}"
        end
      end
    }

    help_cmd = lambda { |l, rest_of_line|
      @cmd_dict.each_key { |k| puts sprintf('%10s ', k) + @cmd_dict[k][:description]}
    }

    add_dot_command('help', 'What you are looking at.', &help_cmd)
    add_command('?', 'Shorthand for .help', &help_cmd)

    add_command('!', 'Ka-boom') { raise 'Ka-boom' } # dev mode

    def self.command_list
      @cmd_dict.keys - ['.', '?', '!']
    end

  # end module Command
  end

# end module Lispish
end

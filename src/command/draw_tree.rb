# frozen_string_literal: true

# make this dynamically inherit from either Nameless or Named ?
class DrawTerm
  attr_reader :type
  attr_reader :name
  attr_reader :left
  attr_reader :rite

  def initialize(type, expr1, expr2 = nil)
    @type = type
    case @type
    when Lispish::TermType::VAR
      @name = expr1
    when Lispish::TermType::ABS, Lispish::TermType::APP
      @left = expr1
      @rite = expr2
    else raise "bad term `#{term.type}' in Term constructor"
    end
  end

  def r
    if type == Lispish::TermType::VAR
      'v'
    elsif type == Lispish::TermType::ABS
      'λ'
    elsif type == Lispish::TermType::APP
      '@'
    end
  end

  attr_accessor :n
  attr_accessor :y
  attr_accessor :pos
  attr_accessor :mod
  attr_accessor :parent

  private

  @@nexts = []
  @@offset = 0
  def self.nexts
    @@nexts
  end

  def self.clone_it(subject, depth = 0, pos = 0)
    novel = # conditional assignment
      if subject.type == Lispish::TermType::VAR
        DrawTerm.new(Lispish::TermType::VAR, subject.name_prop.dup)
      else
        DrawTerm.new(subject.type, clone_it(subject.left_prop, depth + 1, pos - 1), clone_it(subject.right_prop, depth + 1, pos + 1))
      end
    if @@nexts[depth].nil?
      @@nexts[depth] = 1
    else
      @@nexts[depth] += 1
    end
    novel.n = @@nexts[depth] - 1
    novel.y = depth
    novel.pos = pos
    # puts "> #{pos} #{@@offset}"
    @@offset = pos if pos < @@offset
    # puts "< #{pos} #{@@offset}"
    novel
  end

  def self.prep_it(subject)
    # p @@offset
    subject.pos += (@@offset*-1) # now always gonna be greater than or equal to 0
    if subject.type == Lispish::TermType::VAR
    else
      prep_it(subject.left)
      prep_it(subject.rite)
    end
  end
end

def draw_tree(term)
  draw_me = DrawTerm::clone_it(term)
  DrawTerm::prep_it(draw_me)
  # p draw_me
  layout_tree([draw_me])
  draw_me.parent = draw_me
  print_tree([draw_me])
end

def nil.r
  'v'
end

def produce_nodes_for_layout(terms)
  new_nodes = []
  terms.each do |term|
    if term.type == Lispish::TermType::VAR
    elsif term.type == Lispish::TermType::ABS
      term.left.pos += term.mod
      new_nodes.push(term.left)
      term.rite.pos += term.mod
      new_nodes.push(term.rite)
    elsif term.type == Lispish::TermType::APP
      term.left.pos += term.mod
      new_nodes.push(term.left)
      term.rite.pos += term.mod
      new_nodes.push(term.rite)
    end
  end
  new_nodes
end

def produce_nodes_for_print(terms)
  new_nodes = []
  terms.each do |term|
    if term.type == Lispish::TermType::VAR
    else
      term.left.parent = term
      new_nodes.push(term.left)
      term.rite.parent = term
      new_nodes.push(term.rite)
    end
  end
  new_nodes
end

def print_tree(terms)
  j = 0
  terms.each_with_index do |term, i|
    while j < term.pos
      #            #
      #     @      #
      #   / | \    #
      # v   v   v  #
      print '   ' + ' ' # 3 spaces plus margin right
      j += 1
    end
    if term.pos < term.parent.pos
      print '   ' + '/' # overprint right
    elsif term.pos == term.parent.pos
      print ' | ' + ' '
    else
      print "\b\\   " + ' ' # overprint left
    end
    j = term.pos + 1
  end
  puts

  j = 0
  terms.each_with_index do |term, i|
    while j < term.pos
      print '   ' + ' ' # 3 spaces plus margin right
      j += 1
    end
    # print sprintf("%02d ",term.pos)
    print sprintf(" %s ",term.r) + ' ' # ditto
    j = term.pos + 1
  end
  puts

  new_nodes = produce_nodes_for_print(terms)
  return if new_nodes.length == 0
  print_tree(new_nodes)
end

def layout_tree(terms)
  terms.each_with_index do |term, i|
    diff = 0
    if i > 0
      if term.pos == terms[i-1].pos
        diff = 1
      elsif term.pos < terms[i-1].pos # 1 2 -> 3 2
        diff = (terms[i-1].pos - term.pos) + 1
      end
    end
    term.mod = diff
    term.pos += diff
  end

  new_nodes = produce_nodes_for_layout(terms)
  return if new_nodes.length == 0
  layout_tree(new_nodes)
end

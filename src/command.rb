# frozen_string_literal: true

# this dir
#
# $:.unshift File.dirname(__FILE__)
# $LOAD_PATH.unshift File.dirname(__FILE__)

# below dir
#
# $LOAD_PATH.unshift(File.dirname(__FILE__) + '/' + File.basename(__FILE__, '.rb'))

# below dir
#
# glob all of command
# top level module
=begin
  module Lispish
    # this namespace
    module Command
      Dir.glob(
        File.expand_path(File.basename(__FILE__, '.rb') + '/*.rb', __dir__)
      ).sort.each(&method(:require))
    end
  end
=end

Lispish::Include.globular

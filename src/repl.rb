# frozen_string_literal: true

require_relative 'reader' # parslet lexer = read input & lex all in one
require_relative 'evaluator' # evaluate parse tree

# https://en.wikipedia.org/wiki/Read–eval–print_loop

# Minimal definition is:

# (define (REPL env)
#   (print (eval env (read)))
#   (REPL env) )

# In this implementation:
#
#      print = converts the eval'd expression to a string
#      eval  =
#      read  =
# once/loop  =

# REPO (read, eval, print, once)
# &
# REPL (read, eval, print, loop)

# abstract language
class AbstractLanguage
  attr_accessor :features
  def initialize(features)
    @features = features
  end

  def READ(_)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def EVAL(_)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def PRINT(_)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  # 
  def LOOP(lines, &block)
    raise "input of type #{lines.class} is not iterable?" unless lines.respond_to? :each_with_index
    lines.each_with_index do |str, idx|
      begin
        ONCE(str, &block)
      rescue => e # catch anything that happens in ONCE
        # TODO: user controllable backtrace that works with logging
        # puts e.gotcha.backtrace
        L::ERR[e.gotcha]
        # raise "… on line ⫶#{idx+1}⫶ #{str} #{e.message}"
        raise "… on line [#{idx + 1}] #{str.strip}"
      end
    end
  end

  #
  def ONCE(str) # implicit &block
    yield str
  #
  rescue Parslet::ParseFailed => parse_failure
    puts parse_failure.parse_failure_cause.children[0]

    raise Lispish::ParseFailure.new(str, parse_failure) # only prints outermost, not helpful
  #
  rescue SystemStackError => stack_failure # Fix grammar and/or Parslet or user code :(
    raise Lispish::StackFailure.new(str, stack_failure)
  #
  rescue Lispish::FeatureFailed => feature_failure
    raise Lispish::FeatureFailure.new(str, feature_failure)
  # catch and reraise so Exception doesn't catch it
  rescue Lispish::Abort
    raise Lispish::Abort
  # catch everything else
  rescue => other_failure
    if str.blank? # blank input
    end
    raise Lispish::InternalFailure.new(str, other_failure) # this happens in dev mode, test/rspec these :/
  end

  # 
  def TYPE_CHECK(_)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

###
#
#
#
###
class StandardLambdaCalculus < AbstractLanguage
  def initialize(features={})
    super(features)
  end

  def READ(context, str)
    # unvarnished parse tree (takes two passes with Parslet: parse() and transform())
    # p str

    parse_tree = Lispish::Reader.uslcl(str)
    # p parse_tree

    named_term = Lispish::AST::Untyped.parse_tree_to_term(context, parse_tree) # 1.
    # p named_term

    named_term
  end

  def double_check(context, quasi_bruijn_term)
    if @features[:double_check]
      throwaway_term = quasi_bruijn_term.send(:clone_me)
      term = Lispish::AST::Untyped.evaluate(context, throwaway_term, @features[:strategy])
      another_quasi_bruijn_term = Lispish::AST::Untyped.bruijnify(context, term)
      nameless_repr = Lispish::NamelessTerm.construct_from_named_term(another_quasi_bruijn_term)
      print "#{nameless_repr} # "
    end
  end

  def EVAL(context, named_term)
    return nil if named_term.nil?

    # the fun starts here
    if @features[:surreal]
      quasi_bruijn_term = Lispish::AST::Untyped.bruijnify(context, named_term) # 2a.
      double_check(context, quasi_bruijn_term)
      intermediate_term = surreal_term = Lispish::SurrealTerm.construct_from_named_term(quasi_bruijn_term) # 2b.
    elsif @features[:named]
      # naive(?) way
      # no second step :)
      intermediate_term = named_term
    elsif @features[:nameless]
      # more sophisticated(?)
      quasi_bruijn_term = Lispish::AST::Untyped.bruijnify(context, named_term) # 2a.
      double_check(context, quasi_bruijn_term)
      intermediate_term = nameless_term = Lispish::NamelessTerm.construct_from_named_term(quasi_bruijn_term) # 2b.
      # check term and other term give equivalent/identical results
    else
      raise Lispish::FeatureFailed.new
    end

    # same for all
    term = Lispish::AST::Untyped.evaluate(context, intermediate_term, @features[:strategy]) # 3.

    shush do # other_term.to_tree unless term.nil?
    end if $VERBOSE

    # TYPE_CHECK(term) # oh ho ho
    term
  end

  def PRINT(exp)
    # exp.to_s # nil gets turned into ''
    exp.repr
  end

  def ONCE(line)
    super(line)
  end

  def LOOP(lines)
    super(lines)
  end
end

###
#
#
#
###
class CalculusWithAssignment < AbstractLanguage
  def initialize(features = {})
    super(features)
  end

  def READ(context, str)
    # foo ← bar
    # parse tree
    parse_tree = Lispish::Reader.ucwal(str)
    # have to figure out how to handle null/empty input at the object and meta level
    alias_term = Lispish::AST::Alias.parse_tree_to_term(context, parse_tree)

    alias_term
  end

  def EVAL(context, alias_term)
    shush do # term.to_tree unless term.nil?
    end if $VERBOSE

    # TYPE_CHECK(term) # oh ho ho

    # not really evaluating, just aliasing/defining, sticking it into the context
    Lispish::AST::Alias.define(context, alias_term)
  end

  def PRINT(exp)
    exp.to_s
  end

  def ONCE(line)
    super(line)
  end

  def LOOP(lines)
    super(lines)
  end
end

###
#
#
#
###

require 'active_support'
require 'active_support/core_ext/object/blank'

def mirror; end
add_reflective_form(':t', ':mirror') #

alias λ lambda
$repl_env = {}

# these are all untyped, the old way
#
# should use binding.lang (or something) BasicObject < Language ? eval in context of lang binding?
#
REP       = λ { |lang, _line| lang.ONCE(_line) { |line| lang.PRINT(lang.EVAL($repl_env, lang.READ($repl_env, line))) } }
REPL      = λ { |lang, lines| lang.LOOP(lines) { |line| lang.PRINT(lang.EVAL($repl_env, lang.READ($repl_env, line))) } }

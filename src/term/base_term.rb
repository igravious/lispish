# frozen_string_literal: true

# outer module
module Lispish
  # base term class
  class BaseTerm
    attr_reader :left
    attr_reader :right
    def initialize(expr1, expr2)
      @left  = expr1
      @right = expr2
    end

    # left accessors
    def left_prop
      @left
    end

    def left_prop=(new_left) # as if
      @left = new_left
    end

    # right accessors
    def right_prop
      @right
    end

    def right_prop=(new_right) # as if
      @right = new_right
    end
  end
  # end class BaseTerm

=begin
  BaseTerm = Struct.new(:left_prop, :right_prop)
=end
end
# end module Lispish

# frozen_string_literal: true

# TODO: rubocop Stlye/Hash stuffs
# https://rubocop.readthedocs.io/en/latest/cops_style/#stylehasheachmethods
# https://rubocop.readthedocs.io/en/latest/cops_style/#stylehashtransformkeys
# https://rubocop.readthedocs.io/en/latest/cops_style/#stylehashtransformvalues

require 'typesafe_enum'
require_relative './named_term'

# outer module
module Lispish
  # extra enum to identify this term in an AST
  class TermType < TypesafeEnum::Base
    new :ALIAS
  end

  # extra term used to build the alias AST with expressions
  # like foo ← bar
  class AliasTerm < NamedTerm
    def initialize(type, expr1, expr2)
      # if ALIAS
      @type  = type # useless
      @left  = expr1
      @right = expr2
    end

    def visible(mode)
      case @type
      when TermType::ALIAS # duh
        '·←·'[mode]
      else raise "bad term `#{term.type}' in visible"
      end
    end

    def stringify(is_outermost, show_closed, de_bruijn)
      # p is_outermost # true
      # p show_closed   # false
      # p de_bruijn     # false
      x = closed_symbol(show_closed)
      case type
      when TermType::ALIAS # redundant
        a = @left.stringify(false, show_closed, de_bruijn)
        b = '=' # '←'
        c = @right.stringify(is_outermost, show_closed, de_bruijn)
        x + a + b + c
      else raise "bad term `#{term.type}' in stringify"
      end
    end

    # def to_json
    #   # [MetaTerm.actual_to_json(self)]
    #   super()
    # end

    # grok the alias and delegate
    def self.actual_to_json(term)
      case term.type
      when Lispish::TermType::ALIAS # wow
        {
          name: 'alias',
          children: [
            Term.actual_to_json(term.left_prop),
            Term.actual_to_json(term.right_prop)
          ]
        }
      else raise "bad term `#{term.type}' in to_json"
      end
    end
  end

  # how to have a class that's the union of all the <Term classes ?
end
# end module Lispish

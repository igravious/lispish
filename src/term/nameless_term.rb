# frozen_string_literal: true

require 'typesafe_enum'
require_relative './named_term'

# main outer module
module Lispish
  # use De Bruijn indexing scheme
  # see ch. 6 “Nameless Representation of Terms” of TAPL
  # and see https://en.wikipedia.org/wiki/De_Bruijn_index
  # and see https://ttic.uchicago.edu/~pl/classes/CMSC336-Winter08/lectures/lec4.pdf
  # can be 0-based or 1-based which accounts for some variance
  class NamelessTerm < HousekeepingTerm
    # no term type :)
    # the syntax is the type

    attr_accessor :refer_to
    attr_accessor :lost_name

    def initialize(expr1, expr2)
      super(expr1, expr2)
    end

    def self.konstruct(_, expr1, expr2)
      # could double-check
      NamelessTerm.new(expr1, expr2).tap { |t|
        t.closed_prop = expr1.nil? ? expr2.closed_prop : expr1.closed_prop && expr2.closed_prop
      }
    end

    def de_bruijnified?
      true
    end

    # these are changing to a chiral “triple”
    # [      ] = EPS
    # [e1,   ] = VAR / CONST - VARs if untyped, CONSTs if typed
    # [  , e2] = ABS (de Bruijn enc)
    # [e1, e2] = APP

    def type
      if @left.nil? and @right.nil?
        Lispish::TermType::EPS
      elsif @right.nil?
        Lispish::TermType::VAR
      elsif @left.nil?
        Lispish::TermType::ABS
      else
        Lispish::TermType::APP
      end
    end

    def abstraction
      return nil unless type == TermType::ABS

      # needs to be a nil (Nothing) that has a to_s
      { binder: nil, lambda_term: right_prop } # argument: body:
    end

    def application
      return nil unless type == TermType::APP

      { applicator: left_prop, applicand: right_prop }
    end

    def variable
      return nil unless type == TermType::VAR

      {name: left_prop}
    end

    def short_circuit?
      false
    end

    #
    # shift me
    #

    # Inner shift() function
    def self.shift_recurse(by, value_, from)
      case value_.type
      when Lispish::TermType::VAR
        new_distance = value_.left_prop + (value_.left_prop >= from ? by : MINIMUM_SCANNING_DISTANCE)
        raise 'you gotta be kidding (new distance)' if new_distance.nil?

        n = NamelessTerm.new(
          new_distance,
          nil
        )
        n.closed_prop = (new_distance >= MINIMUM_SCANNING_DISTANCE)
        n
      when Lispish::TermType::APP
        n = NamelessTerm.new(
          l = shift_recurse(by, value_.left_prop, from),
          r = shift_recurse(by, value_.right_prop, from)
        )
        n.closed_prop = l.closed_prop && r.closed_prop
        n
      when Lispish::TermType::ABS
        new_abs = shift_recurse(by, value_.right_prop, from + LEVEL_STEPPING)
        raise 'you gotta be kidding (new abs shift)' if new_abs.nil?

        n = NamelessTerm.new(
          nil,
          new_abs
        )
        n.lost_name = value_.lost_name
        n.closed_prop = new_abs.closed_prop
        n
      end
    end

    # Outer shift() function
    def self.shift(by, value_)
      shift_recurse(by, value_, INDEX_FROM)
    end

    def var_matcher(value_, depth)
      # p "var catcher"
      if depth == left_prop
        NamelessTerm::shift(depth, value_)
      else
        self
      end
    end

    def alpha_conversion_helper(value_, depth)
      self
    end

    def reset_alpha ; end # no-op

    def alpha_converted? ; false end # always false

    def self.construct_from_named_term(named_term)
      # guard
      raise 'patent stuff and nonsense' unless named_term.de_bruijnified?

      @@unique_names={}
      recursively_construct_from_named_term(named_term)[0]
    end

    # FIXME: modularize
    def self.recursively_construct_from_named_term(named_term)
      binders = []
      copy_over = named_term.instance_variables - %i[@type @left @right @name @de_bruijn @bind_to] # :)
      novel =
        case named_term.type
        when TermType::EPS
          NamelessTerm.new(nil, nil)
        when TermType::VAR
          if named_term.instance_variable_defined? :@distance # bound
            L::DBG['var0'] # how do i describe this scenario ?

            mr_var = NamelessTerm.new(named_term.distance, nil)
            # mr_var = NamelessTerm.new(NamelessTerm.new(named_term.name_prop, nil), nil)
            binders.push([mr_var, named_term.bind_to])
            mr_var
          elsif instance_variable_defined? :@num_free  # never visited ?
            raise 'uh oh'

            raise 'bad number of free variables?' unless 1 == @num_free

            L::DBG['var1'] # how do i describe this scenario ?
            # NamelessTerm.new(named_term.name_prop, nil)
          else
            L::DBG['var2'] # how do i describe this scenario ?
            # NamelessTerm.new(named_term.name_prop, nil)

            # consistent renaming
            unless @@unique_names.include?(named_term.name_prop)
              @@unique_names[named_term.name_prop] = (FIRST_FREE_DE_BRUIJN - @@unique_names.size)
            end
            NamelessTerm.new(@@unique_names[named_term.name_prop], nil)
          end
        when TermType::ABS
          novel_binders_pair = recursively_construct_from_named_term(named_term.right_prop)
          # where do we need to point back from?
          mr_lambda = NamelessTerm.new(
            nil,
            novel_binders_pair[0]
          )
          delete_them = []
          novel_binders_pair[1].each_with_index do |match_binders, i|
            next unless named_term == match_binders[1]

            match_binders[0].refer_to = mr_lambda

            delete_them.push(i)
          end
          mr_lambda.lost_name = named_term.left_prop.name_prop
          delete_them.each{ |i| novel_binders_pair[1].delete_at(i) }
          binders = novel_binders_pair[1]
          mr_lambda
        when TermType::APP
          left_novel_binders_pair  = recursively_construct_from_named_term(named_term.left_prop)
          right_novel_binders_pair = recursively_construct_from_named_term(named_term.right_prop)
          binders = left_novel_binders_pair[1]+right_novel_binders_pair[1]
          NamelessTerm.new(
            left_novel_binders_pair[0],
            right_novel_binders_pair[0]
          )
        end
      copy_over.each { |i| novel.instance_variable_set(i, named_term.instance_variable_get(i)) } # :)
      [novel, binders] #
    end

    def main_substitute
      # const substitute = (value_, node) => {
      #   return shift(-1, subst(shift(1, value_), node));
      # };
      node = left_prop
      new_value = NamelessTerm::shift(1, right_prop)
      new_value = HousekeepingTerm::substitute(new_value, node.right_prop, INDEX_FROM)
      NamelessTerm::shift(-1, new_value)
    end

    ###
    #
    ###

    private

    def clone_me
      novel = # conditional assignment
        if type == TermType::EPS
          NamelessTerm.new(nil, nil)
        elsif type == TermType::VAR
          NamelessTerm.new(left_prop, nil)
        elsif type == TermType::ABS
          NamelessTerm.new(nil, right_prop.send(:clone_me))
        elsif type == TermType::APP
          NamelessTerm.new(left_prop.send(:clone_me), right_prop.send(:clone_me))
        else
          raise 'bad clone, for goodness sake'
        end
      copy_over = instance_variables - novel.instance_variables # :)
      copy_over.each { |i| novel.instance_variable_set(i, instance_variable_get(i)) } # :)
      novel
    end
  end
  # end class Nameless

  # how to have a class that's the union of all the <Term classes ?
end
# end module Lispish

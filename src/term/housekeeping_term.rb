# frozen_string_literal: true

require_relative './base_term'

# main outer module
module Lispish
  # base term class

  INDEX_FROM = 0 # make this a feature
  MINIMUM_SCANNING_DISTANCE = INDEX_FROM
  FIRST_FREE_DE_BRUIJN = MINIMUM_SCANNING_DISTANCE - 1
  LEVEL_STEPPING = 1

  # keeps track of information that is implicit in the structure of an AST
  # we don't technically need to keep track but it's more efficient
  # in a separate class from the base class to show it's ancillary
  class HousekeepingTerm < BaseTerm
    def initialize(expr1, expr2)
      super(expr1, expr2)
    end

    #
    # inspect
    #

    def concise(i_var_sym)
      i_var = instance_variable_get(i_var_sym)
      i_var_sym.to_s + '=' +
        case i_var
        when Integer
          '#' + i_var.to_s
        when TrueClass
          'true'
        when FalseClass
          'false'
        when HousekeepingTerm # hmm
          case i_var.type
          when Lispish::TermType::VAR
            "&v#{i_var}" # dedicated method
          when Lispish::TermType::ABS
            '&λ(…)'
          when Lispish::TermType::APP
            '&@(…)'
          else
            raise "more gee whizz: #{i_var.class}"
          end
        when NilClass
          'nil'
        # when Parslet::Slice
        #   "'#{self.instance_variable_get(i_var)}'"
        when String # from α conversion
          '"' + i_var + '"'
        when Symbol
          ':' + i_var.to_s
        when SurrealNumber
          '$' + i_var.to_i.to_s
        else
          raise "gee whizz: #{i_var.class}"
        end
    end

=begin
    def t
      case type
      when TermType::EPS
        'ε'
      when TermType::VAR
        'v'
      when TermType::APP
        '@'
      when TermType::ABS
        'λ'
      else raise 'bad type' # TODO: make this a thingy
      end
    end
=end

    alias_method :orig_inspect, :inspect

    # #<Lispish::NamelessTerm:0x0055b500c06c90 @left="y", @right=nil, @track_the_level=0, @is_closed=false, @num_free=2>
    def inspect(&block)
      return orig_inspect if block.nil?

      "#<#{self.class}:#{object_id.to_s(16)}:#{type.t} " +
        (instance_variables - [:@type]).collect { |i| concise(i) }.join(', ') +
        '>'
    end

    #
    # @conclude
    #

    def there_is_conclusion?
      instance_variables.include?(:@conclude)
    end

    def conclude
      @conclude
    end

    def set_conclude(c) # #tap ?
      @conclude = c
      self
    end

    #
    # @num_free
    #
    def there_is_a_number_of_free_variables?
      instance_variables.include?(:@num_free)
    end

    def number_of_free_variables
      return @num_free if there_is_a_number_of_free_variables?

      nil
    end

    def number_of_free_variables=(new_num_free)
      # extreme paranoia – don't duck type
      # p new_num_free.class
      raise 'what in the tarnation?' unless new_num_free.is_a? Integer and new_num_free >= 0 #

      @is_closed = new_num_free.zero?
      @num_free  = new_num_free
    end

    def dec_number_of_free_variables
      # extreme paranoia – don't duck type
      raise 'nothing to decrease' unless there_is_a_number_of_free_variables?

      @num_free -= 1 unless @num_free.zero?
      @is_closed = @num_free.zero? # might be closed
      @num_free
    end

    def inc_number_of_free_variables
      # extreme paranoia – don't duck type
      raise 'nothing to increase' unless there_is_a_number_of_free_variables?

      @num_free += 1
      @is_closed = false # can't be closed
      @num_free
    end

    #
    # @is_closed
    #
    def there_is_a_closed_prop?
      instance_variable_defined?(:@is_closed)
    end

    def closed?
      return @is_closed if there_is_a_closed_prop?

      nil
    end

    def closed_prop
      # if !there_is_a_closed_prop?
      #   binding.ish
      # end
      @is_closed
    end

    def closed_prop=(new_closed)
      @is_closed = new_closed
    end

    #
    # @track_the_level
    #
    def the_level
      @track_the_level
    end

    def the_level=(new_level)
      @track_the_level = new_level
    end

    #
    # muy importante
    #

    def set_closed_flags
      free_variables {}
    end

    def set_closed_flags_cond
      free_variables{} unless there_is_a_number_of_free_variables?
    end

    # not used anywhere !
    def meh_is_closed?
      free_variables(false) {}.empty? #
    end

    #
    # huh
    #

    def in_eta_form?
      # (λ x . (P x)) => P
    end

    #
    # the magic
    #

    # (λx.2 ∗ x + 1)3 = 2 ∗ 3 + 1 = 7
    # (λx.M)N = M[x := N], (for β-reduction)
    def free_variables(save = true, depth = INDEX_FROM, &anon_yield_block)
      vars = []
      self.the_level = depth
      case type
      when TermType::VAR
        # vars.push(@name)
        vars.push(self)
        # when TermType::ALIAS
        # nothing? must be closed
        # $env[@name.to_sym].send(:free_variables, save)
      when TermType::ABS
        vars = @right.send(:free_variables, save, depth + LEVEL_STEPPING, &anon_yield_block)
        # vars.delete_if { |n| @left.name == n }
        # is this a binder ?
        delete_them = []
        vars.each_with_index do |v, i|
          next unless strike(v)

          yield(v, self)

          delete_them.push(i)
        end
        delete_them.each { |i| vars.delete_at(i) }
      when TermType::APP
        vars  = @left.send(:free_variables, save, depth, &anon_yield_block)
        rvars = @right.send(:free_variables, save, depth, &anon_yield_block)
        vars += rvars # .dup
      else raise 'bad term in free variables'
      end
      self.number_of_free_variables = vars.length if save # @close if @num_free = 0
      vars
    end

    #
    # Representation Stuff !!!
    #

    # https://stackoverflow.com/questions/29696717/define-the-method-each-for-nilnilclass
    def nil.to_string(_)
      ''
    end

    def nil.repr
    end

    def to_string(is_outermost)
      to_s(is_outermost).to_s
    end

    def repr
      # puts(self.inspect {})
      to_s
    end

    def to_s(is_outermost=true)
      self.type.to_sub_term(self, is_outermost)
    end

    #
    # Substitution Stuff !!!
    #

    # Named and Nameless substitute() function !!!
    def self.substitute(value_, node, orig_node) # orig_node is to depth
      return node if node.short_circuit?

      new_node = transmogrify(value_, node, orig_node)

      case node.type
      when Lispish::TermType::VAR
        new_node
      when Lispish::TermType::APP
        l = substitute(value_, new_node.left_prop,  orig_node)
        r = substitute(value_, new_node.right_prop, orig_node)
        n = node.class.konstruct(TermType::APP, l, r)
        # n.closed_prop = l.closed_prop && r.closed_prop
        n
      when Lispish::TermType::ABS
        l = new_node.left_prop
        r = substitute(value_, new_node.right_prop, orig_node + LEVEL_STEPPING)
        n = node.class.konstruct(TermType::ABS, l, r)
        # n.closed_prop = l.nil? ? r.closed_prop : l.closed_prop && r.closed_prop
        n
      end
    end

    # this preps node so that the subst logic is simple
    def self.transmogrify(value_, node, carry)
      case node.type
      when TermType::VAR
        node.var_matcher(value_, carry)
      when TermType::ABS
        node.alpha_conversion_helper(value_, carry)
      when TermType::APP
        node
      else raise 'bad term in transmogrify()'
      end
    end
    #
    # :=]
    #
  end
  # end class HousekeepingTerm
end
# end module Lispish

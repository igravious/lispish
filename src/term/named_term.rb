# frozen_string_literal: true

require 'typesafe_enum'
require_relative './housekeeping_term'

# FIXME: make sure closed_prop doesn't become nil

# outer module
module Lispish
  # should be defined in Term, and MetaTerm, DeBruijnTerm, and so on (maybe)
  #
  # https://github.com/dmolesUC/typesafe_enum
  #
  class TermType < TypesafeEnum::Base
    # @type    syntax   name          description
    #                   Epssilon      Null term
    new :EPS, 'epsilon' do
      def t
        'ε'
      end
    end

    #          (λx.M)   Abstraction   Function definition (M is a lambda term).
    #                                 The variable x becomes bound in the expression.
    new :ABS do
      def to_sub_term(term, _)
        # puts(term.inspect {})
        binder = term.abstraction[:binder].to_string(true)
        lambda_term = term.abstraction[:lambda_term].to_string(true)
        'λ' + binder + '.' + lambda_term
      end

      def t
        'λ'
      end
    end
    #          (M N)    Application   Applying a function to an argument.
    #                                 M and N are lambda terms.
    new :APP do
      def to_sub_term(term, is_outermost)
        # puts(term.inspect {})
        out = term.left_prop.type == TermType::APP
        applicator = term.application[:applicator].to_string(out)
        applicand = term.application[:applicand].to_string(false)
        app = applicator + ' ' + applicand
        is_outermost ? app : '(' + app + ')'
      end

      def t
        '@'
      end
    end
    #          x        Variable      A character or string representing
    #                                 a parameter or mathematical/logical value.
    new :VAR do
      def to_sub_term(term, _)
        # term.variable[:name].to_string
        term.variable[:name].to_s # FIXME: nameless to_string()
      end
    end

    def t
      'v'
    end
  end

  class NamedTerm < HousekeepingTerm
    attr_reader :name
    attr_reader :type

    # attr_reader :closed # A term is closed if there are no unbound variables.
    # for de bruijn index/term transition stuff – inherit
    attr_accessor :bind_to
    attr_accessor :distance
    attr_accessor :shadow_id

    # these are in HousekeepingTerm
    # attr_accessor :the_level
    # attr_accessor :num_free

    def initialize(type, expr1, expr2 = nil)
      @type = type
      case @type
      when TermType::VAR
        @name = expr1
      when TermType::ABS, TermType::APP
        super(expr1, expr2)
      else raise "bad term `#{term.type}' in Term constructor"
      end
    end

    def self.konstruct(type, expr1, expr2)
      # could double-check
      NamedTerm.new(type, expr1, expr2).tap { |t|
        t.closed_prop = expr1.closed? && expr2.closed?
      }
    end

    #
    # ~~~
    #
    def variable
      return nil unless @type == TermType::VAR

      { name: name_prop }
    end

    def variable?
      !variable.nil?
    end

    def abstraction
      return nil unless @type == TermType::ABS

      { binder: left_prop, lambda_term: right_prop } # argument: body:
    end

    def abstraction?
      !abstraction.nil?
    end

    def application
      return nil unless @type == TermType::APP

      { applicator: left_prop, applicand: right_prop }
    end

    def to_sub_term

    end

    #
    # should recurse ?
    #
    def make_de_bruijn
      @de_bruijn = :so_do_i
    end

    def de_bruijnified?
      instance_variables.include?(:@de_bruijn)
    end

    # def to_tree

    # def find_max_level

    # def visible

    # def self.actual_find_max_level

    # def self.print_ws

    # def self.horizontal

    # def self.actual_to_tree

    # def to_json

    # def self.actual_to_json

    # def to_x

    # def to_xtr

    # def to_s

    # def to_str

    # def closed_symbol

    # def var

    # def abs

    # def app

    # def stringify

    # def boxify

    # closed accessors
=begin
    def there_is_a_closed_prop?
      instance_variable_defined?(:@closed)
    end

    def closed_prop
      @closed
    end

    def closed_prop=(new_closed)
      @closed = new_closed
    end
=end

    # name accessors
    def name_prop
      @name
    end

    def name_prop=(new_name)
      @name = new_name.dup
    end

    def strike(term)
      left_prop.name_prop == term.name_prop
    end

    # private_class_method

    # need to implement various styles(?) (stratgeies(?)

    def short_circuit?
      closed? # short circuit
    end

    def var_matcher(value_, orig_node)
      # p "var_matcher – named"
      if name_prop == orig_node.name_prop
        # puts "== #{self.inspect {}}"
        value_.send(:clone_me)
      else
        # puts "!= #{self.inspect {}}"
        self
      end
    end

    def +(_) self ; end # side-stepping 

    ###
    #
    ###
    def free_var?(name)
      return false if closed?

      case @type
      when TermType::VAR
        @name == name
      when TermType::APP
        @left.free_var?(name) || @right.free_var?(name)
      when TermType::ABS
        @left.name != name && @right.free_var?(name)
      else raise 'bad term in free_var?()'
      end
    end

    def self.alpha_conversion(term1, term2) # should be private?
      # my way
      # return t1.name.nil? ? t1.left_prop.name + '•' : t1.name + '•' # !

      # lci way
      s = 'a'
      while term1.free_var?(s) || term2.free_var?(s)
        i = s.length - 1
        loop do
          s[i].next
          break if i.positive? || (s[i]) != 'z'

          i -= 1
        end
        s += 'a' if i.positive?
      end
      s
      # sestoft lamreduce way
      # ?
    end

=begin
    def old_alpha_conversion_helper(node, orig_node)
      return unless free_var?(node.left_prop.name_prop) && node.right_prop.free_var?(orig_node.name_prop)

      puts 'α'
      puts(self.inspect {})
      puts(node.inspect {})
      puts(orig_node.inspect {})

      alpha_value = Lispish::NamedTerm.new(Lispish::TermType::VAR, NamedTerm.alpha_conversion(self, node.right_prop)) # α-conversion
      alpha_value.closed_prop = false
      HousekeepingTerm.right_substitute(alpha_value, node, node.left_prop)

      # right_substitute(value_, m, orig_node)
      # m.closed_prop = m.right_prop.closed_prop
      # node.closed_prop = node.right_prop.closed_prop

      node.left_prop.name_prop = alpha_value.name_prop
    end
=end

    def needs_alpha_conversion?(value_, orig_node)
      !value_.closed? &&
        !right_prop.closed? &&
        value_.free_var?(left_prop.name_prop) &&
        right_prop.free_var?(orig_node.name_prop)
    end

    # def alpha_converted
    #   @alpha_converted = :exists
    #   self
    # end

    def alpha_converted?
      # instance_variable_defined?(:@alpha_converted)
      !@@alpha_.empty?
    end

    def reset_alpha
      @@alpha_ = {} # shared across all terms
      send(:shadow_me)
    end

    def show_alpha
      # @@alpha_.each{ |n| puts "node:#{n[:node].object_id}: #{n[:node]} – value:#{n[:value].object_id}: #{n[:value]} – orig_node:#{n[:orig_node].object_id}: #{n[:orig_node]} – new_node: #{n[:new_node]} – alpha_value: #{n[:alpha_value]}"}
      # p shadow_id
      if @@alpha_.key?(shadow_id)
        @@alpha_[shadow_id].show_alpha
      else
        case type
        when TermType::VAR
          name_prop
        when TermType::ABS
          'λ' + left_prop.show_alpha + '.' + right_prop.show_alpha
        when TermType::APP
          '(' + left_prop.show_alpha + ' ' + right_prop.show_alpha + ')'
        end
      end
    end

    # should be either a ShadowTerm inherited from NamedTerm or a per named_term instance mixin
    def store_alpha(address, alpha_node)
      # @@alpha_.push(node)
      @@alpha_[address] = alpha_node
    end

    def alpha_conversion_helper(value_, orig_node)
      return self unless needs_alpha_conversion?(value_, orig_node)

      new_name = NamedTerm.alpha_conversion(value_, self.right_prop) # possible α-conversion
      alpha_value = Lispish::NamedTerm.new(Lispish::TermType::VAR, new_name)
      alpha_value.closed_prop = false
      l = alpha_value
      r = HousekeepingTerm::substitute(alpha_value, self.right_prop, self.left_prop)
      n = NamedTerm.new(TermType::ABS, l, r)
      n.left_prop = l
      n.closed_prop = l.closed? && r.closed?
      # n.set_conclude 0
      # L::WRN['α-conversion']
      # L::WRN["node:#{object_id}: #{self} – value:#{value_.object_id}: #{value_} – orig_node:#{orig_node.object_id}: #{orig_node} – new_node: #{n} – alpha_value: #{alpha_value}"]
      # store_alpha({node: self, value: value_, orig_node: orig_node, new_node: n, alpha_value: alpha_value})
      store_alpha(object_id, n.send(:clone_me)) # if instrumenting this do this but only then
      n
    end

    def main_substitute
      node = left_prop
      HousekeepingTerm::substitute(right_prop, node.right_prop, node.left_prop)
    end


=begin
    def var_left(value, term)
      term.left_prop = send(:clone_me) if term.left_prop.name_prop == value.name_prop
    end

    def var_right(value, term)
      term.right_prop = send(:clone_me) if term.right_prop.name_prop == value.name_prop
    end
=end

    private

    def clone_me
      novel = # conditional assignment
        if @type == TermType::VAR
          NamedTerm.new(TermType::VAR, @name.dup)
        else
          NamedTerm.new(@type, @left.send(:clone_me), @right.send(:clone_me))
        end
      copy_over = instance_variables - novel.instance_variables # :)
      copy_over.each { |i| novel.instance_variable_set(i, instance_variable_get(i)) } # :)
      novel
    end

    def shadow_me
      novel = # conditional assignment
        if @type == TermType::VAR
          NamedTerm.new(TermType::VAR, @name.dup)
        else
          NamedTerm.new(@type, @left.send(:shadow_me), @right.send(:shadow_me))
        end
      novel.shadow_id = object_id
      copy_over = instance_variables - novel.instance_variables # :)
      copy_over.each { |i| novel.instance_variable_set(i, instance_variable_get(i)) } # :)
      novel
    end
  end
  # class NamedTerm
end
# module Lispish

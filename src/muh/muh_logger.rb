# frozen_string_literal: true

# https://stackoverflow.com/questions/34247939/how-to-set-per-filetype-tab-size
# Shortcut is: Command Palette (Ctrl⇧P) then: Preferences: Configure Language Specific Settings

require 'rubygems'
require 'bundler/setup'
# https://ruby-doc.org/stdlib-2.4.0/libdoc/logger/rdoc/Logger.html
require 'logger'
# https://ruby-doc.org/stdlib-2.4.0/libdoc/singleton/rdoc/index.html
require 'singleton'

# :-
class MuhLogger
  include Singleton

  attr_accessor :mr_logger_man
  attr_accessor :mono_formatter

  def initialize
    @mr_logger_man = Logger.new(STDERR)

    original_formatter = Logger::Formatter.new
    @mono_formatter = proc { |severity, datetime, progname, obj|
      case severity
      when 'INFO'
        original_formatter.call(severity, datetime, progname, obj)
      when 'WARN'
        original_formatter.call(severity, datetime, progname, obj)
      when 'DEBUG'
        original_formatter.call(severity, datetime, progname, obj)
      when 'ERROR'
        original_formatter.call(severity, datetime, progname, obj)
      else
        original_formatter.call(severity, datetime, progname, obj)
      end
    }
    require 'colorize'
    @color_formatter = proc { |severity, datetime, progname, obj|
      case severity
      when 'INFO'
        original_formatter.call(severity, datetime, progname, obj.light_green)
      when 'WARN'
        original_formatter.call(severity, datetime, progname, obj.light_yellow)
      when 'DEBUG'
        original_formatter.call(severity, datetime, progname, obj.light_red)
      when 'ERROR'
        if $DEBUG
          if obj.class == String
            original_formatter.call(severity, datetime, progname, obj.to_s.red)
          else
            original_formatter.call(severity, datetime, progname, obj)
          end
        else
          original_formatter.call(severity, datetime, progname, obj.to_s.red)
        end
      end
    }

    level_info
    color
    # monochrome
    super
  end

  def monochrome
    @mr_logger_man.formatter = @mono_formatter
  end

  def color
    @mr_logger_man.formatter = @color_formatter
  end

  def level_info
    @mr_logger_man.level = Logger::INFO
  end

  def level_warn
    @mr_logger_man.level = Logger::WARN
  end

  def level_debug
    @mr_logger_man.level = Logger::DEBUG
  end

  def level_error
    @mr_logger_man.level = Logger::ERROR
  end

  def level
    @mr_logger_man.level
  end

  def info(msg)
    @mr_logger_man.info(msg)
  end

  def warn(msg)
    @mr_logger_man.warn(msg)
  end

  def debug(msg)
    @mr_logger_man.debug(msg)
  end

  def error(msg)
    @mr_logger_man.error(msg)
  end
end

require 'bundler/setup'

MuhLogger.instance.debug("Created muh logger :/")

# logger.info("Program started")
# logger.warn("Nothing to do!")
# logger.level = Logger::ERROR # for test mode

=begin

⏺ Exception
  ⏺ NoMemoryError
  ⏺ ScriptError
    ⏺ LoadError
    ⏺ NotImplementedError
    ⏺ SyntaxError
  ⏺ SecurityError (1.9: move!)
  ⏺ SignalException
    ⏺ Interrupt
  ⏺ StandardError [ default for plain rescue ]
    ⏺ ArgumentError
    ⏺ EncodingError (1.9)
      ⏺ Encoding::CompatibilityError (1.9)
      ⏺ Encoding::ConverterNotFoundError (1.9)
      ⏺ Encoding::InvalidByteSequenceError (1.9)
      ⏺ Encoding::UndefinedConversionError (1.9)
    ⏺ FiberError (1.9)
    ⏺ IOError
      ⏺ EOFError
    ⏺ IndexError
      ⏺ KeyError (1.9)
      ⏺ StopIteration
    ⏺ LocalJumpError
    ⏺ Math::DomainError (1.9)
    ⏺ NameError
      ⏺ NoMethodError
    ⏺ RangeError
      ⏺ FloatDomainError
    ⏺ RegexpError
    ⏺ RuntimeError [ default for plain raise ]
    ⏺ SecurityError (1.8: move!)
    ⏺ SystemCallError
      ⏺ Errno::*
    ⏺ SystemStackError (1.8: move!)
    ⏺ ThreadError
    ⏺ TypeError
    ⏺ ZeroDivisionError
  ⏺ SystemExit
  ⏺ SystemStackError (1.9: move!)
  ⏺ fatal

=end

# https://ruby-doc.org/core-2.5.0/Exception.html
#
module Lispish

  # prep to catch and unpack
  class Rescue < StandardError
    attr_accessor :gotcha
    attr_accessor :code
    def initialize(code, gotcha)
      @code = code
      @gotcha = gotcha
      super("#{gotcha.class} ⮞ #{gotcha.message}")
    end
  end

  # these catch and unpack
  class ParseFailure < Rescue
    def initialize(code, f)
      if !f.cause.nil?
        puts f.cause.ascii_tree
        super(code, f.cause.children[0])
      elsif !f.parse_failure_cause.nil?
        puts f.parse_failure_cause.ascii_tree
        super(code, f.parse_failure_cause.children[0])
      else
        super(code, f)
      end
    end
  end

  class StackFailure < Rescue
    def initialize(code, f)
      super(code, f)
    end
  end

  class EvalFailure < Rescue
    def initialize(code, f)
      super(code, f)
    end
  end

  class InternalFailure < Rescue
    def initialize(code, f)
      super(code, f)
    end
  end

  class FeatureFailure < Rescue
    def initialize(code, f)
      super(code, f)
    end
  end

  # for long jumps
  class AlphaConversion < StandardError
    attr_accessor :node
    def initialize(node)
      @node = node
      super("AlphaConversion ⮞ #{node}")
    end
  end

  # these don't
  class Abort < RuntimeError
  end

  class EvalFailed < RuntimeError
  end

  class FeatureFailed < RuntimeError
  end
end

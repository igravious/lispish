# frozen_string_literal: true

# thanks to Raganwald for much of this:
# https://github.com/raganwald-deprecated/homoiconic/blob/master/2009-03-07/surreal.md
#
# and to Tøndering for much of the rest of this:
# https://www.tondering.dk/download/sur.pdf
#
# and to Jeff Anderson
# https://github.com/peawormsworth/PySurreal/blob/master/surreal/__init__.py

# these are only surreal integers for the moment
# https://ruby-doc.org/core-2.5.0/Integer.html
# https://ruby-doc.org/core-2.5.0/Struct.html
# https://ruby-doc.org/stdlib-2.7.0/libdoc/set/rdoc/Set.html
# https://medium.com/launch-school/object-oriented-programming-in-ruby-basics-and-definitions-2-2-7307fed1482

def q(obj)
  puts obj.q
end

# class Surreal < Integer
# end

def stup(str)
  puts str
end

# :-
class Container
  def initialize(*args)
    puts '---'
    p args
    puts '+++'
    if args.empty?
      []
    elsif 1 == args.length
      [args.first]
    else
      raise 'Zero or one element in Container for the moment.'
    end
  end
end

Container.class_eval do
  # [] ^ []
  # [] ^ SurrealNumber
  def ^(other)
    s =
      if other.is_a?(Container)
        # s = SurrealNumber.new(self, other)
        # s.birthday = 1 # i know, i know :)
        if other.empty?
          # NULL_SUR
          # NULL_SUR.dup
          if empty?
            SurrealNumber.new(EMPTY_S, EMPTY_S)
          else
            SurrealNumber.new(self, EMPTY_S)
          end
        else
          SurrealNumber.new(self, other)
        end
      elsif other.is_a?(SurrealNumber)
        # s = SurrealNumber.new(self, [other])
        # s.birthday = other.birthday + 1
        SurrealNumber.new(self, [other])
      else
        # duck type :(
        raise Math::DomainError, 'Wrong type fed into Container::^'
      end
    raise Math::DomainError, 'Surreal number is not valid' unless s.valid?

    s
  end
end

EMPTY_S = Container.new.freeze

# Definition 1.  A  surreal  number  is  a  *pair*  of  sets  of  previously  created  surreal  numbers.
# The  sets  are  known  as  the  “left  set”  and  the  “right  set”.
# No  member  of  the  right  set  may  be  less_than_or_equal_to  any  member  of  the  left  set.

# Definition 2.  A  surreal  number  x  is  less_than_or_equal_to  a  surreal  number  y
# if  and  only  if
# y  is  less_than_or_equal_to  no  member  of  x’s  left  set,
# and  no  member  of  y’s  right  set  is  less  than  or  equal  to  x.

# :-
# SurrealNumber = Struct.new(:numbers_to_my_left, :numbers_to_my_right) do
# end
# https://www.rubydoc.info/gems/rubocop/RuboCop/Cop/Style/StructInheritance
#
# TODO: can SurrealNumber be_a? Integer ?
#
class SurrealNumber < Struct.new(:numbers_to_my_left, :numbers_to_my_right)
  include Comparable

  attr_accessor :birthday

  def q
    "#(#{q})"
  end

  def quote
    "#{numbers_to_my_left.quote}^#{numbers_to_my_right.quote}"
  end

  def conway
    "#{CONWAY_[0]}#{numbers_to_my_left.conway}#{CONWAY_[1]}#{numbers_to_my_right.conway}#{CONWAY_[2]}"
  end

=begin
  def to_s
    self.conway
  end
=end

  # super inefficient at the moment
  def to_i
    # return '#'

    return 0 if self == NAUGHT_

    return ltz if self <= NAUGHT_

    rtz # must be >= NAUGHT_
  end

  def ltz(acc = NAUGHT_, res = 0)
    acc = acc.dec
    res -= 1
    return res if self == acc

    # we might have overshot
    raise Math::DomainError, 'Surreal number is not an integer' if self >= acc

    ltz(acc, res)
  end

  def rtz(acc = NAUGHT_, res = 0)
    acc = acc.inc
    res += 1
    return res if self == acc

    # we might have overshot
    raise Math::DomainError, 'Surreal number is not an integer' if self <= acc

    rtz(acc, res)
  end

  ###

  def not_to_the_left_of?(other)
    numbers_to_my_right.none? { |right| other.not_to_the_left_of?(right) } &&
      other.numbers_to_my_left.none? { |left| left.not_to_the_left_of?(self) }
  end

  def not_to_the_right_of?(other)
    other.not_to_the_left_of?(self)
  end

  def ordered?
    SurrealNumber.ordered?(numbers_to_my_left, numbers_to_my_right)
  end

  def self.ordered?(on_my_left, on_my_right)
    on_my_left.all? do |left|
      on_my_right.none? do |right|
        left.not_to_the_left_of?(right)
      end
    end
  end

  ###
  # ∀ x ∈ L , ∀ y ∈ R : y ≤ x .
  #
  # Notational convention. The symbol ≤ means “less than or equal to”.
  # The symbol ≰ means “not less than or equal to”.
  # Thus, x ≰ y is equivalent to ¬ ( x ≤ y ) .
  def valid?
    numbers_to_my_left.all?(&:valid?) && numbers_to_my_right.all?(&:valid?) && ordered?
  end

  ###

  def ==(other)
    other.is_a?(SurrealNumber) && not_to_the_left_of?(other) && not_to_the_right_of?(other)
  end

  def <=(other)
    other.is_a?(SurrealNumber) && not_to_the_right_of?(other)
  end

  def >=(other)
    other.is_a?(SurrealNumber) && not_to_the_left_of?(other)
  end

  # Comparable
  def <=>(other)
    return 0 if self.==(other)

    return -1 if self.<=(other)

    return 1 if self.>=(other)

    raise Math::DomainError, 'Not comparing with a Surreal Number'
  end

  ###

  def to_the_right_of?(other)
    not_to_the_left_of?(other) && !not_to_the_right_of?(other)
  end

  def to_the_left_of?(other)
    not_to_the_right_of?(other) && !not_to_the_left_of?(other)
  end

  def map_left(other)
    numbers_to_my_left.map { |left| left + other }
  end

  def map_right(other)
    numbers_to_my_right.map { |right| right + other }
  end

  def null?
    numbers_to_my_left.empty? && numbers_to_my_right.empty?
  end

  def +(other)
    res =
      case other
      when Integer
        general_plus(other.to_surreal) # hmm
      when SurrealNumber
        general_plus(other)
      end
    # transform into canonical form
    res.canonical
  end

=begin
  def +(other)
    res =
      case other
      when Integer
        integer_plus(other.to_surreal) # hmm
      when SurrealNumber
        integer_plus(other)
      end
    # transform into canonical form
    res
  end
=end

  def integer_plus(other)
    return self if other.zero?

    return other if zero?

    i = self
    if other <= NAUGHT_
      other.abs.times { i = i.sub_one }
    else
      other.times { i = i.add_one }
    end

    i
  end

  def abs
    return NAUGHT_ if zero?

    return -self if self <= NAUGHT_

    self
  end

  def times
    pointer = numbers_to_my_left.first
    until pointer.nil?
      yield
      pointer = pointer.numbers_to_my_left.first
    end
  end

  def zero?
    self == NAUGHT_
  end

  def general_plus(other)
    # (numbers_to_my_left.map { |left| left + other } | other.numbers_to_my_left.map { |left| left + self }) ^
    #   (numbers_to_my_right.map { |right| right + other } | other.numbers_to_my_right.map { |right| right + self })

    left1 = numbers_to_my_left.map { |left| left.general_plus(other) }
    left2 = other.numbers_to_my_left.map { |left| left.general_plus(self) }
    right1 = numbers_to_my_right.map { |right| right.general_plus(other) }
    right2 = other.numbers_to_my_right.map { |right| right.general_plus(self) }

    left3 = left1 | left2
    right3 = right1 | right2

    res = left3 ^ right3

    res
  end

  def inc
    # self.+(POZ_ONE)
    integer_plus(POZ_ONE)
  end

  def dec
    # self.+(NEG_ONE)
    integer_plus(NEG_ONE)
  end

  def add_one
    if numbers_to_my_right.empty?
      # n >= 0
      SurrealNumber.new(Container.new(self), EMPTY_S)
    else
      raise Math::DomainError, 'Not a simple Surreal Integer' unless numbers_to_my_left.empty?

      # n < 0
      numbers_to_my_right.first
    end
  end

  def sub_one
    if numbers_to_my_left.empty?
      # n <= 0
      SurrealNumber.new(EMPTY_S, [self])
    else
      raise Math::DomainError, 'Not a simple Surreal Integer' unless numbers_to_my_right.empty?

      # n > 0
      numbers_to_my_left.first
    end
  end

  def -(other)
    (case other
    when Integer
      general_minus(other.to_surreal)
    when SurrealNumber
      general_minus(other)
    end).canonical
  end

  def canonical
    to_i.to_surreal
  end

=begin
  def -(other)
    res =
      case other
      when Integer
        integer_minus(other.to_surreal) # hmm
      when SurrealNumber
        integer_minus(other)
      end
    # transform into canonical form
    res
  end
=end

  def integer_minus(other)
    return self if other.zero?

    return -other if zero?

    i = self
    if other >= NAUGHT_
      other.times { i = i.sub_one }
    else
      other.abs.times { i = i.add_one }
    end

    i
  end

  def general_minus(other)
    res = self.+(-other) # unary negation
    # efficient transform to canonical form :/
    return SurrealNumber.new(EMPTY_S, EMPTY_S) if res.zero?

    res
  end

  def -@
    numbers_to_my_right.map(&:-@) ^ numbers_to_my_left.map(&:-@)
  end

  # SurrealNumber ^ []
  # SurrealNumber ^ SurrealNumber
  def ^(other)
    me = [self]
    s =
      if other.is_a?(Array)
        # s = SurrealNumber.new([self], other)
        # s.birthday = @birthday + 1
        SurrealNumber.new(me, other)
      else
        # s = SurrealNumber.new([self], [other])
        # s.birthday = @birthday + other.birthday
        SurrealNumber.new(me, [other])
      end
    raise Math::DomainError, 'Surreal number is not valid' unless s.valid?

    s
  end
end

# NULL_SUR = SurrealNumber.new(EMPTY_S, EMPTY_S).freeze

=begin
Array.class_eval do
  # [] ^ []
  # [] ^ SurrealNumber
  def ^(other)
    s =
      if other.is_a?(Array)
        # s = SurrealNumber.new(self, other)
        # s.birthday = 1 # i know, i know :)
        if other.empty?
          # NULL_SUR
          # NULL_SUR.dup
          if empty?
            SurrealNumber.new(EMPTY_S, EMPTY_S)
          else
            SurrealNumber.new(self, EMPTY_S)
          end
        else
          SurrealNumber.new(self, other)
        end
      else
        # s = SurrealNumber.new(self, [other])
        # s.birthday = other.birthday + 1
        SurrealNumber.new(self, [other])
      end
    raise Math::DomainError, 'Surreal number is not valid' unless s.valid?

    s
  end
end
=end

=begin
def nil.+(_)
  []
end

def nil.conway
  ''
end
=end

# :-
class Array
  def quote
    map(&:quote).to_s
  end

  def conway
    empty? ? '' : self[0].conway
  end
end

CONWAY_ = '⟨|⟩'

NAUGHT_ = (EMPTY_S ^ EMPTY_S).freeze # {∅ | ∅}  =  ⟨|⟩    ≡ 0   or in English  “zero”
POZ_ONE = (NAUGHT_ ^ EMPTY_S).freeze # {0 | ∅}  =  ⟨0|⟩   ≡ 1   “one”
NEG_ONE = (EMPTY_S ^ NAUGHT_).freeze # {∅ | 0}  =  ⟨|0⟩   ≡ -1  “negative one” in the US and “minus one” in the UK/IE
POZ_TWO = (POZ_ONE ^ EMPTY_S).freeze # {1 | ∅}  =  ⟨1|⟩   ≡ 2   “two”
NEG_TWO = (EMPTY_S ^ NEG_ONE).freeze # {∅ |-1}  =  ⟨|-1⟩  ≡ -2  “minus two”

REV_POZ_TWO = (EMPTY_S ^ POZ_ONE).freeze # = 0 btw
REV_NEG_TWO = (NEG_ONE ^ EMPTY_S).freeze # = 0 btw

# kind of crap you can't do the following the same way you can for nil
=begin
def 1.to_surreal
  NAUGHT_
end
=end

# :-
class Integer
  def to_surreal
    sur = NAUGHT_
    i = 0
    if zero?
      ;
    elsif positive?
      while i < self
        sur = sur ^ EMPTY_S
        i += 1
      end
    else # self is negative (obviously)
      while i > self
        sur = EMPTY_S ^ sur
        i -= 1
      end
    end
    sur
  end
end

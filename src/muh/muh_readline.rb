# frozen_string_literal: true

require 'readline'

require_relative '../command'

module Lispish

  module Readline

    COMMAND_LIST = Lispish::Command::command_list.sort

    COMPLETION_PROC = proc { |s|
      a = COMMAND_LIST.grep(/^#{Regexp.escape(s)}/)
      if a.empty?
        a = ['λ'] if s == '\\'
        a = ['⊥'] if (s == '_|_') || (s == '_¦_')
      end
      a
    }

  end

end

Readline.basic_word_break_characters = "\"'`@$><=;&{("

# Readline.completion_append_character = " "

Readline.completion_proc = Lispish::Readline::COMPLETION_PROC

module Lispish

  module Readline

    STTY_SAVE = `stty -g`.chomp
    PROMPT = '▷ '

    def self.prompt(str)
      print str + "\n" + PROMPT
    end

    @history_loaded = false
    HISTORY_FILE = "#{ENV['HOME']}/.mal_history"
    IS_TTY = ENV['INPUTRC'] != '/dev/null'

    def self.readline(prompt = PROMPT)
      if IS_TTY && !@history_loaded && File.exist?(HISTORY_FILE)
        @history_loaded = true
        if File.readable?(HISTORY_FILE)
          File.readlines(HISTORY_FILE).each { |l| ::Readline::HISTORY.push(l.chomp) }
        end
      end

      return nil unless line = ::Readline.readline(prompt, true)

      if IS_TTY && File.writable?(HISTORY_FILE)
        File.open(HISTORY_FILE, 'a+') { |f| f.write(line + "\n") }
      end
      line
    end

  end

end

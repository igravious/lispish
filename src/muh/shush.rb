# frozen_string_literal: true

# http://mislav.net/2011/06/ruby-verbose-mode/
# these methods are already present in Active Support
unless Kernel.respond_to? :silence_warnings
  module Kernel
    def silence_warnings
      with_warnings(nil) { yield }
    end

    def with_warnings(v_flag)
      old_verbose = $VERBOSE
      $VERBOSE = v_flag
      yield
    ensure
      $VERBOSE = old_verbose
    end

    def silence_debug
      with_debug(nil) { yield }
    end

    def with_debug(d_flag)
      old_debug = $DEBUG
      $DEBUG = d_flag
      yield
    ensure
      $DEBUG = old_debug
    end

    def shush
      with_both(nil, nil) { yield }
    end

    def with_both(v_flag, d_flag)
      old_verbose = $VERBOSE
      $VERBOSE = v_flag
      old_debug = $DEBUG
      $DEBUG = d_flag
      yield
    ensure
      $VERBOSE = old_verbose
      $DEBUG = old_debug
    end
  end
end

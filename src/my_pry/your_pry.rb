# frozen_string_literal: true

require 'bundler/setup'
require 'binding_of_caller'
require 'pry'

# set up binding.ish to wrap binding.pry
class Binding
  SKIP = 10 # 10 with shush {}, 6 without

  # all so we can say binding.ish
  def ish
    # shush {Binding::ish}
    shush { Binding::ish }
  end
  class << self
    attr_accessor :interrupted
    def ish
      @interrupted = true
      the_caller = binding.of_caller(SKIP-4)
      Thread.handle_interrupt(Timeout::Error => :never) do
        the_caller.eval('binding.pry')
      end
    end
  end
end

class Pry
  class Command
    class IshBacktrace < Pry::ClassCommand
      match 'ish-backtrace'
      group 'Context'
      description 'Show the backtrace for the Pry session.'

      banner <<-BANNER
        Usage: ish-backtrace [OPTIONS] [--help]
        Show the backtrace for the position in the code where Pry was started. This can
        be used to infer the behavior of the program immediately before it entered Pry,
        just like the backtrace property of an exception.
        NOTE: if you are looking for the backtrace of the most recent exception raised,
        just type: `_ex_.backtrace` instead.
        See: https://github.com/pry/pry/wiki/Special-Locals
      BANNER

      def process
        text = bold('Better Backtrace:')
        text << "\n--\n"
        len  = _pry_.backtrace.length
        text << _pry_.backtrace.pop(len-Binding::SKIP).join("\n")
        _pry_.pager.page(text)
      end
    end

    Pry::Commands.add_command(Pry::Command::IshBacktrace)
  end
end
# frozen_string_literal: true

# glob all of muh
# top level module
module Lispish
  # this namespace
  module Muh
    Lispish::Include.globular
  end
end

# frozen_string_literal: true

begin # silence_warnings do
  require 'bundler/setup' # Set up gems listed in the Gemfile.
  require 'parslet'
  # require 'parslet/accelerator'
  # require 'parslet/convenience'
  # require 'parslet/rig/rspec'
  #
  # http://rsec.herokuapp.com/
  # http://rsec.herokuapp.com/ref
  # https://github.com/luikore/rsec/wiki/From-PEG-to-rsec
  #
  # https://tree-sitter.github.io/tree-sitter/
end

# monkey patch Parslet::Atoms::Context.lookup
class Parslet::Atoms::Context
  alias_method :stash, :lookup
  def lookup(obj, pos)
    if $DEBUG
      chunk = ($str[pos..-1]).nil? ? (' '*$str.length) : ($str[pos..-1]).strip.rjust($str.length)
      puts "[#{sprintf("%02d",pos)}] #{chunk} #{obj.class} :: #{obj.inspect}"
      # binding.ish if pos == ?
    end
    @cache[pos][obj]
  end
end

# EBNF for a concrete lambda calculus syntax
#
# top                 → ε | forms_of_expression
#
# forms_of_expression → abstraction | application | variable
#
# argument            → variable | parens_abs | parens_app
#
# parens_app          → l_parens application r_parens
#
# application         → argument concat argument
#
# concat              → ' '
#
# parens_abs          → l_parens abstraction r_parens
#
# abstraction         → abs_rule
#
# abs_rule            → abstraction_op currying forms_of_expression
#
# abstraction_op      → '\\') | 'λ'
#
# currying            → l_bracket var_list r_bracket) | var_list dot
#
# var_list            → variable var_list | ε
#
# dot                 → '.'
#
# variable            → literal
#
# literal             → /[^[^[:alnum:]][λ]]/
#
# l_parens            → '('
#
# r_parens            → ')'
#
# l_bracket           → '['
#
# r_bracket           → ']'

#
module Lispish
  #
  class LambdaCalculusLexer < Parslet::Parser

    rule(:calculus) do
      ε.as(:eps) | forms_of_expression
    end

    rule(:forms_of_expression) do
      abstraction.as(:abs) | application | variable.as(:var)
    end

    rule(:argument) do
      variable.as(:var) | parens_abs.as(:abs) | parens_app
    end

    # rule(:function) do
    #   variable.as(:var) | parens_abs.as(:abs) | application
    # end

    # rule(:func_or_arg) do
    #   variable.as(:var) | parens_abs.as(:abs) | parens_app # | application
    # end

    # app

    rule(:parens_app) do
      # l_parens >> application | ε.as(:eps) >> r_parens
      l_parens >> application >> r_parens
      # l_parens >> application | space? >> r_parens
    end

    # https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/First_Steps
    # $ infix function application (f $ x is the same as f x, but right-associative instead of left)

    #    rule(:application) {
    #     function.as(:l) >> spc.as(:o) >> argument.as(:r)
    #    }
    #
    #    rule(:func_or_arg) {argument | function}

    # (1) can't do "(a b c )" at the mo, borks on "c )" treating " " as infix
    # (2) and can't do "(\x.x)a" at the mo, has to be "(\x.x) a"
    # (3) as well as can't do "λx.(f(x x))", must put a space like "λx.(f (x x))"
    # (3) also can't differentiate left and right parse
    rule(:application) do
      # infix_expression(func_or_arg, [str(' '), 1, :left]) { |l, o, r| { app: { l: l, o: o, r: r } } }
      infix_expression(argument, [str(' '), 1, :left]) { |l, o, r| { app: { l: l, o: o, r: r } } }
    end

    # abs

    rule(:parens_abs) do
      # l_parens >> abstraction | ε.as(:eps) >> r_parens
      l_parens >> abstraction >> r_parens
    end

    rule(:abstraction) do
      abs_rule # >> type_construction?
    end

    rule(:abs_rule) do
      abstraction_op >> dynamic {
        # puts 'abs'
        currying.as(:curry) >> forms_of_expression.as(:body) # >> (alternative_op >> abs_rule).repeat # recursion
      }
    end

    rule(:abstraction_op) do
      space? >> (str('\\') | str('λ')).as(:lambda)
    end

    # rule(:alternative_op) do
    #   space? >> str('|')
    # end

    rule(:currying) do # madrasable – LOL, i kill me
      (l_bracket >> var_list >> r_bracket) | (var_list >> dot) # | ε.as(:EPSILON) # why void ?
    end

    # rule(:var_list) {
    #	  variable >> (spc >> variable).repeat
    # }

    rule(:dot) do
      space? >> str('.')
    end

    # these things [] {} ()

    # []
    rule(:l_bracket) do # yeah, i know, square bracket
      space? >> str('[')
    end
    rule(:r_bracket) do
      space? >> str(']')
    end

    # {}
    rule(:l_brace) do
      space? >> str('{')
    end
    rule(:r_brace) do
      space? >> str('}')
    end

    # ()
    rule(:l_parens) do
      space? >> str('(')
    end
    rule(:r_parens) do
      space? >> str(')')
    end

    # var

    # variables are placeholder labels
    rule(:variable) do
      dynamic {
        #  puts 'lit'
        literal
      }
      # >> type_construction?
      # first time you use the type it constructs it,
      # the rest it pattern matches
    end

    rule(:literal) do # yo, check it, that's not even on t'internet –
      # all unicode letter 'cept λ
      # they are variable *names* so i'll need another mechanism to define
      # numeric and punctuation and wot have you (maybe aliases?)
      # figure this out!
      #
      # space? always before match.repeat

      space? >> alnum_cept_special.as(:lit) | surreal.as(:sur) # matched_parens.as(:lit)
    end

    rule(:surreal) do
      zero.as(:zero) | left_blank | right_blank | str('{') >> surreal.as(:left) >> str('|') >> surreal.as(:right) >> str('}')
    end

    rule(:right_blank) do
      str('{') >> surreal.as(:left) >> str('|') >> str('}')
    end

    rule(:left_blank) do
      str('{') >> str('|') >> surreal.as(:right) >> str('}')
    end

    rule(:zero) do
      str('ε') | (str('{') >> str('|') >> str('}'))
    end

    rule(:alnum_cept_special) { match(/[^[^[:alnum:]][λε]]/).repeat(1) }

    # retarded way to do this :/
    rule(:matched_parens) { match['('].repeat(1).capture(:capt) >> dynamic { |s,c| str(')' * c.captures[:capt].length) } }

    rule(:ε)      { any.absent? }

    rule(:eof)    { any.absent? }

    rule(:spc)    { match('\s').repeat(1) }

    rule(:space?) { spc.maybe }

    def read(str)
      $str = str # @@str :)
      $stack = []
      # printf sprintf("%16s",(str.gsub(/\s+$/,'·').gsub(/^\s+/,'·'))) # U+00b7
      # result = $parser.parse_with_debug(str.strip, :reporter => Parslet::ErrorReporter::Deepest.new)
      # binding.ish

      str = Lispish::Reader.lexer_fixme(str)
      prsr = parse(str.strip, reporter: Parslet::ErrorReporter::Contextual.new)
      result = Lispish::Reader.trans.apply(prsr)

      result
    end
  end

  class UntypedLambdaCalculusLexer < LambdaCalculusLexer
    rule(:var_list) do
      variable >> (spc >> variable).repeat
    end
=begin
    rule(:var_list) do
      variable.capture(:capt) >> dynamic { |s, c| # stream, context
        puts c.captures[:capt]
        (spc >> variable).repeat
      }
    end
=end
  end

  # could merge the two?
  # have a flag or whatever for type checking
  # in which case there must be not type constructs
  # the implicit or explicit sub-flags
  class TypedLambdaCalculusLexer < LambdaCalculusLexer
    rule(:var_list) do
      variable >> type_construct >> (spc >> variable >> type_construct).repeat
    end

    # rule(:type_construction?) {
    # (type_op >> space? >> match('[A-Z]').repeat(1).as(:type)).maybe
    #
    rule(:type_construct) do
      type_op >> literal.as(:type)
    end

    rule(:type_op) do
      space? >> str(':')
    end
  end

  class UntypedStandardLambdaCalculusLexer < UntypedLambdaCalculusLexer
    root :top

    rule(:top) do
      calculus # .as(:expr)
    end
  end

  # FIXME: not implemented
  class SimplyTypedLambdaCalculusLexer < TypedLambdaCalculusLexer
    root :top

    rule(:top) do
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end
  end

  # line oriented for the moment
  class UntypedCalculusWithAssignmentLexer < UntypedLambdaCalculusLexer
    root :top

    rule(:top) do
      # can't decide whether infix or prefix
      # what to use for stack notation?
      (
        ( variable.as(:ident) >> alias_op | alias_op >> variable.as(:ident) ) >> calculus.as(:expr)
      ).maybe >> space? >> comment.as(:throwaway).maybe
    end

    rule(:alias_op) do
      space? >> (str('<-') | str('←')).as(:alias)
    end

    rule(:comment) do
      str('#') >> any.repeat
    end
  end
end

# require_relative 'triadic'

module Lispish
  class Transform < Parslet::Transform
    # rule(:abs => sequence(:x)) {
    rule(abs: subtree(:sub)) do
      # {:lambda=>"λ"@2, :curry=>[{:lit=>"x"@5}, {:lit=>"y"@7}, {:lit=>"z"@9}], :body=>{
      #
      # {:lambda=>"λ"@2, :curry=>[{:lit=>"x"@5} :body=> {:lambda=>'λ', :curry=>{:lit=>"y"@7}, :body=>{:lambda=>'λ', :curry=>{:lit=>"z"@9}, :body=>{
      # 
      # puts 'transform'
      if sub[:curry].is_a? Array
        while sub[:curry].length > 1
          # tmp_body = { abs: { lambda: 'λ', **sub[:curry].pop, body: sub[:body] } }
          tmp_body = { abs: { lambda: 'λ', curry: sub[:curry].pop, body: sub[:body] } }
          sub[:body] = tmp_body
        end
        sub[:curry] = sub[:curry][0]
      elsif sub[:curry].is_a? Hash

      else
        raise 'Oh woah is me' # :-[] test cases
      end
      { abs: sub }
    end

    # Zerothness Firstness Secondness Thirdness

=begin
    rule(abs: subtree(:sub)) {
      # puts 'abs'
      # p self
      { abs: sub }
    }
=end

    rule(app: subtree(:sub)) {
      # puts 'app'
      # p self
      { app: sub }
    }

    rule(var: subtree(:sub)) {
      # puts 'var'
      # p self
      { var: sub }
    }
  end
end

# “oh look, we got ourselves a top-level module with the same name as the project”
module Lispish
  # “oh look, we got ourselves a reader”
  module Reader
    #
    # https://www.rubydoc.info/gems/rubocop/RuboCop/Cop/Style/ClassVars
    #
    # @@ inside `def` @ inside `module`
    #
    @@uslcl = Lispish::UntypedStandardLambdaCalculusLexer.new
    @@stlcl = Lispish::SimplyTypedLambdaCalculusLexer.new
=begin
    A = ::Parslet::Accelerator # for making what follows a bit shorter
    @@ucwal = A.apply(Lispish::UntypedCalculusWithAssignmentLexer.new,
      A.rule( (A.str(:x).absent? >> A.any).repeat ) { GobbleUp.new(x) })
=end
    @@ucwal = Lispish::UntypedCalculusWithAssignmentLexer.new
    @@trans = Lispish::Transform.new

    def self.trans
      @@trans
    end

    def self.uslcl(str)
      @@uslcl.read(lexer_fixme(str))
    end

    def self.ucwal(str)
      @@ucwal.read(lexer_fixme(str))
    end

    def self.lexer_fixme(str)
      # FIXME: lexer
      # oof , hack to until lexer is fixed
      # get rid of spaces before )        does not break any logic, fixes some typos
      str = str.gsub(/\s+\)/, ')')
      # insert a space between )(         does not break any logic, fixes some typos
      str = str.gsub(/\)\(/, ') (')
      # insert a space after ) if none already and if not eol or another )
      str.gsub(/\)([^ \)$])/, ') \1')
      # .gsub(/\(/,' (')
    end
  end
end

# this is some weird hunch/intuition i have
def add_reflective_form(symbol, method)
  # :-[]
end

# frozen_string_literal: true

# glob all of wherever we were called from
#
# top level module
module Lispish
  # this namespace
  module Include
    require 'bundler/setup'
    require 'binding_of_caller'

    def self.globular
      file = File.basename(binding.of_caller(1).source_location[0])
      Dir.glob(
        File.expand_path(File.basename(file, '.rb') + '/*.rb', __dir__)
      ).sort.each(&method(:require))
    end

    def foofoo
      # p binding.of_caller(0)
      # p binding.of_caller(1)
      # p binding.of_caller(2)
      # "foofoo"
      # #<Binding:0x00005573f948ee88 @iseq=<RubyVM::InstructionSequence:foofoo@/home/groobiest/Code/lispish/src/include.rb:12>>
      # #<Binding:0x00005573f948e8c0 @iseq=<RubyVM::InstructionSequence:<main>@src/ish.rb:0>>

      # p binding.of_caller(0).source_location
      # p binding.of_caller(1).source_location
      # p binding.of_caller(2).source_location
      # "foofoo"
      # ["/home/groobiest/Code/lispish/src/include.rb", 14]
      # ["src/ish.rb", 15]
    end
  end
end

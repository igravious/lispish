# frozen_string_literal: true

require_relative File.join('..', 'term') # termology

# main outer module
module Lispish
  # Abstract Syntax Tree
  module AST

    class EvaluationStrategy < TypesafeEnum::Base
      new :NORMAL_ORDER
      new :APPLICATIVE_ORDER
    end

    # https://classes.soe.ucsc.edu/cmps112/Spring03/readings/lambdacalculus/syntax.html
    #
    # guaranteed to be well-formed by the time it gets here
    # so the checks are super redundant sanity checks :)
    module Untyped
      def self.parse_tree_to_term(context, tree)
        expression(context, tree)
      end

      # telemetry ?
      # TODO: move into parse_tree_to_term because we're walking the tree there anyway
      def self.bruijnify(context, named_term)
        quasi_bruijn_term = named_term.send(:clone_me)
        quasi_bruijn_term.make_de_bruijn
        quasi_bruijn_term.send(:free_variables) { |v, t| # "variable", binding position
          v.bind_to = t
          # t.the_level is always smaller than v.the_level
          # -1 cuz it's based
          # TODO: INDEX_FROM
          # eg. (2-1)-1 => 0     { for (\x.x) }
          v.distance = (v.the_level - t.the_level) - Lispish::LEVEL_STEPPING
        }
        quasi_bruijn_term
      end

      # telemetry ?
      def self.evaluate(context, term, strategy)
        term.set_closed_flags_cond # should be here or outside?
        counter = 0 # term = term.send(:clone_me)
        L::INF['[' + format('%02d', counter) + "] #{term}"]
        loop do
          GC.start
          shadow_term = term.reset_alpha # not used in nameless obviously
          new_term = beta_reduction(context, term, strategy)
          raise Lispish::Abort if abort_eval? # should be callback?

          if term.alpha_converted?
            counter += 1
            L::INF['[' + format('%02d', counter) + "] #{shadow_term.show_alpha}"]
          end
          counter += 1
          break if new_term.conclude.zero?

          L::INF['[' + format('%02d', counter) + "] #{new_term}"]
          term = new_term
        end
        term
      end

      # private_class_method

      # TODO: tidy up even more
      def self.beta_reduction(context, term, strategy = Lispish::AST::EvaluationStrategy::NORMAL_ORDER, depth=0)
        raise "closed borked: #{term.inspect}" unless term.there_is_a_closed_prop?

        case term.type
        # when Lispish::NilTerm
        #   term.set_conclude 0
        when Lispish::TermType::VAR
          term.set_conclude 0
        when Lispish::TermType::ABS
          # ignore η-reduction for the moment
          # https://wiki.haskell.org/Eta_conversion
          if term.in_eta_form? # (λ x . (P x)) => P
            eta_result = term.abstraction[:lambda_term].application[:applicator]
            eta_result.set_conclude 1
          else
            term.right_prop = beta_reduction(context, term.right_prop, strategy, depth + 1)
            term.set_conclude term.right_prop.conclude
            term
          end
        when Lispish::TermType::APP
          # substitution in thingies in @left ?? - see lci
          if strategy == Lispish::AST::EvaluationStrategy::NORMAL_ORDER.ord
            if term.left_prop.type == Lispish::TermType::ABS
              term = term.main_substitute
              term.set_conclude 1
              return term
            end

            term.left_prop = beta_reduction(context, term.left_prop, strategy) # normal order = leftmost outermost redex
            t = term.left_prop.conclude.zero? ? (term.right_prop = beta_reduction(context, term.right_prop, strategy) ; term.right_prop.conclude) : 1
            term.set_conclude t
          elsif strategy == Lispish::AST::EvaluationStrategy::APPLICATIVE_ORDER.ord
            term.right_prop = beta_reduction(context, term.right_prop, strategy, depth + 1) # applicative order = leftmost innermost redex
            if term.right_prop.conclude.zero?
              if term.left_prop.type == Lispish::TermType::ABS
                term.main_substitute.set_conclude 1
              else
                term.left_prop = beta_reduction(context, term.left_prop, strategy, depth + 1)
                term.set_conclude term.left_prop.conclude
              end
            else
              term.set_conclude 1
            end
          else # do this once somewhere else
            raise "What the tarnation? strategy is #{strategy}"
          end
        end
      end

      ###
      #
      # for parse_tree_to_term
      #
      # TODO: decorate parse tree with de Bruijn indexes?
      #
      ###

      # variable or constant
      def self.variable(context, whence, var)
        Lispish::AST.malformed(whence, var) unless var.key?(:lit)
        name = var[:lit]
        if context.key?(name.to_sym)
          context[name.to_sym].send(:clone_me)
        else
          # Parslet gives you slices (Parslet::Slice) of input as part of your tree.
          # These are essentially strings with line numbers. Here’s how to print that error message:
          #
          # assume that type == "int"@0 - a piece from your parser output
          # line, col = type.line_and_column
          # puts "Sorry. Can't have #{type} at #{line}:#{col}!"
          #
          # this is what we lose with .to_s
          # make sure this is what we want
          Lispish::NamedTerm.new(Lispish::TermType::VAR, var[:lit].to_s)
        end
      end

      def self.abstraction(context, abs)
        if abs.key?(:curry) && abs.key?(:body)
          if abs.key?(:lambda)
            e1 = variable(context, 'lambda expects variable', abs[:curry])
            e2 = expression(context, abs[:body])
            Lispish::NamedTerm.new(Lispish::TermType::ABS, e1, e2)
          else Lispish::AST.malformed('missing abstraction operation', abs)
          end
        else Lispish::AST.malformed('bad abstraction', abs)
        end
      end

      def self.application(context, app)
        if app.key?(:l) && app.key?(:r)
          e1 = function(context, app[:l])
          e2 = argument(context, app[:r])
          Lispish::NamedTerm.new(Lispish::TermType::APP, e1, e2)
        else Lispish::AST.malformed('bad application', app)
        end
      end

      def self.function(context, func)
        if func.key?(:var)
          variable(context, 'bad variable in function', func[:var])
        elsif func.key?(:abs)
          abstraction(context, func[:abs])
        elsif func.key?(:app)
          application(context, func[:app])
        else Lispish::AST.malformed('bad function', func)
        end
      end

      def self.argument(context, arg)
        if arg.key?(:var)
          variable(context, 'bad variable in argument', arg[:var])
        elsif arg.key?(:abs)
          abstraction(context, arg[:abs])
        elsif arg.key?(:app)
          application(context, arg[:app])
        else Lispish::AST.malformed('bad argument', arg)
        end
      end

      def self.expression(context, expr)
        if expr.key?(:var)
          variable(context, 'bad variable in expression', expr[:var])
        elsif expr.key?(:app)
          application(context, expr[:app])
        elsif expr.key?(:abs)
          abstraction(context, expr[:abs])
        elsif expr.key?(:eps)
          nil
        else Lispish::AST.malformed('bad expression', expr)
        end
      end
    end
    # end module Untyped
  end
  # end module AST
end
# end module Lispish

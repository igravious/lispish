    #
    # treeeeeees
    #
    # https://llimllib.github.io/pymag-trees/
    # http://www.it.usyd.edu.au/~shhong/comp5048-lec2.pdf
    #

    def to_tree
      HousekeepingTerm.actual_to_tree([self], 1, find_max_level, de_bruijnified?)
    end

    def find_max_level
      HousekeepingTerm.actual_find_max_level(self)
    end

    def visible(mode)
      s = ''
      case self.type
      when TermType::VAR
        # s = @name.is_a?(::Parslet::Slice) ? @name : @name.split(' ')[mode]
        s = var('', self.de_bruijnified?)
      when TermType::APP
        s = '·@·'[mode]
      when TermType::ABS
        s = '·λ·'[mode]
      else raise "bad term `#{term.type}' in visible"
      end
      # s+'['+@the_level.to_s+']'+'('+@num_free.to_s+')'
      if self.is_a? NamedTerm
        if instance_variable_defined? :@bind_to
          [s, '('+self.number_of_free_variables.to_s + '['+@bind_to.the_level.to_s + '/'+self.the_level.to_s+'])']
        else
          [s, '('+self.number_of_free_variables.to_s + ':'+self.the_level.to_s + ')']
        end
      else
        [s, '#']
      end
    end

    def self.actual_find_max_level(term) # private
      (term.is_a? BaseTerm) ? ([actual_find_max_level(term.left_prop), actual_find_max_level(term.right_prop)].max + 1) : 0
    end

    def self.print_ws(ct)
      print ' ' * ct if ct.positive?
    end

    def self.fake_print_ws(ct)
      return ' ' * ct if ct.positive?

      ''
    end

    def self.horizontal(terms, first_spaces, between_spaces, mode = 1)
      new_nodes = []
      s = ''
      s += fake_print_ws(first_spaces)
      terms.each do |t|
        out =
          if t.is_a? BaseTerm
            new_nodes.push(t.left_prop)
            new_nodes.push(t.right_prop)
            t.visible(mode)[1]
          else
            new_nodes.push(nil)
            new_nodes.push(nil)
            ' '
          end
        s += out
        s += fake_print_ws(between_spaces - out.length)
      end
      puts s unless s.blank?
      s = ''
      s += fake_print_ws(first_spaces)
      terms.each do |t|
        out =
          if t.is_a? BaseTerm
            t.visible(mode)[0]
          else
            ' '
          end
        s += out
        s += fake_print_ws(between_spaces - out.length)
      end
      puts s unless s.blank?
      new_nodes
    end

    def self.actual_to_tree(terms, lvl, max_lvl, de_bruijn) # private
      return if terms.empty? || terms.all?(&:nil?)

      floor = max_lvl - lvl
      party_lines = 2**[floor - 1, 0].max
      first_spaces = (2**floor) - 1
      between_spaces = (2**(floor + 1)) # - 1

      # puts "$$$ #{terms.length}"
      new_nodes =
        if de_bruijn
          horizontal(terms, first_spaces, between_spaces, 1)
          # horizontal(terms, first_spaces, between_spaces, 1)
          # horizontal(terms, first_spaces, between_spaces, 2)
        else
          horizontal(terms, first_spaces, between_spaces)
        end

      # puts "£££ #{new_nodes.length}"
      i = 1
      s = ''
      while i <= party_lines
        terms.each_with_index do |_t, j|
          s += fake_print_ws(first_spaces - i)
          unless terms[j].is_a?(BaseTerm)
            s += fake_print_ws(party_lines + party_lines + i + 1)
            next
          end
          s += terms[j].left_prop.is_a?(BaseTerm) ? '/' : ' '
          s += fake_print_ws(i + i - 1)
          s += terms[j].right_prop.is_a?(BaseTerm) ? '\\' : ' '
          s += fake_print_ws(party_lines + party_lines - i)
        end
        i += 1
        puts s unless s.blank?
        s = ''
      end
      # puts "€€€"

      actual_to_tree(new_nodes, lvl + 1, max_lvl, de_bruijn)
    end
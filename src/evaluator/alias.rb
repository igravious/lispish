# frozen_string_literal: true

# TODO: list of todos
# (1) fix code smells
# (2) replace readline with tty-reader ? https://github.com/piotrmurach/tty-reader
# (3) ish interactive interpreter commands

require_relative File.join('..', 'term') # termology

# min outer module
module Lispish
  # Abstract Syntax Tree module
  module AST
    # for expressions with ω ← (\x.x x) (\x.x x) syntax
    module Alias
      def self.define(context, term)
        # if ALIAS
        return nil if term.nil?

        e1 = term.left_prop
        e2 = term.right_prop
        context[e1.name.to_sym] = e2
      end

      # need to sort out context
      def self.parse_tree_to_term(context, tree)
        if tree == '' # blank line
          nil
        elsif tree.key?(:throwaway) # comment
          nil
        elsif tree.key?(:ident) && tree.key?(:expr) # foo ← bar
          e1 = Lispish::AST::Untyped.variable(context, 'slot expects variable', tree[:ident])
          e2 = Lispish::AST::Untyped.expression(context, tree[:expr])
          Lispish::AliasTerm.new(Lispish::TermType::ALIAS, e1, e2) # change to SLOT
        else
          Lispish::AST.malformed('bad alias', tree) # raises exception
        end
      end
    end
    # end module Alias
  end
  # end module AST
end
# end module Lispish

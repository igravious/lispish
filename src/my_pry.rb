# frozen_string_literal: true

# glob all of my_pry
# top level module
module Lispish
  # this namespace
  module MyPry
    Lispish::Include.globular
  end
end
